﻿using growble.sim.Data;
using growble.sim.Object.Blocks;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace growble.Models
{
    public class ObjectsModel
    {
        public static List<ObjectM> objects(string file)
        {
            List<ObjectM> obs = new List<ObjectM>();
            Dictionary<string, string> paths = new Dictionary<string, string>();
            string[] data = File.ReadAllLines(file);

            foreach (var x in data)
            {
                string[] keyvalue = x.Split(' ');
                try
                {
                    paths.Add(keyvalue[0], keyvalue[1]);
                }
                catch (Exception ex)
                {

                }
            }

            Catalog c = new Catalog();

            foreach (var x in c.Items)
            {
                if (paths.ContainsKey(x.Name))
                {
                    obs.Add(new ObjectM() { Image = paths[x.Name], block = x.Name });
                }
            }

            return obs;
        }
    }

    public class ObjectM
    {
        public string Image { get; set; }

        public string block { get; set; }
    }
}