﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Web.Security;

namespace growble.Models
{

    public class RegisterModel
    {
        [MinLength(3)]
        [MaxLength(20)]
        [Required]
        public string Username { get; set; }

        [MinLength(3)]
        [MaxLength(20)]
        [Required]
        public string Password { get; set; }

        [MinLength(3)]
        [MaxLength(200)]
        [Required]
        public string Email { get; set; }
    }

    public class LoginModel
    {
        [MinLength(3)]
        [MaxLength(20)]
        [Required]
        public string Username { get; set; }

        [MinLength(3)]
        [MaxLength(20)]
        [Required]
        public string Password { get; set; }

    }
}
