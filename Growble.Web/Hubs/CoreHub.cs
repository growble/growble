﻿using growble.server;
using growble.sim.Data.Repo;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Web;

namespace growble.Hubs
{
    [HubName("coreHub")]
    public class CoreHub : Hub
    {
        public void Login(string username)
        {
            if (username == this.Context.User.Identity.Name)
            {

                AccountRepo arep = new AccountRepo();
                UserRepo urep = new UserRepo();
                var account = arep.GetUser(username);
                urep.Loggin(new sim.Data.OnlineUser() { Name = account.Name, ConnectionId = this.Context.ConnectionId });
                this.Clients.Caller.loginSuccess(account);
            }
            
        }

        public void GetWorlds()
        {
            WorldRepo wrep = new WorldRepo();
            this.Clients.Caller.startWorlds(wrep.StartWorlds());
        }

        /// <summary>
        /// Join the world
        /// </summary>
        /// <param name="name"></param>
        public void JoinWorld(string name)
        {
            WorldRepo wrep = new WorldRepo();
            var activeWorld = wrep.IsWorldActive(name);
            if (activeWorld != null)
            {
                //Give the player the connection details
                this.Clients.Caller.connectToWorld(activeWorld.Connection);
                
            }
            else
            {
                try
                {

                    ProcessStartInfo ps = new ProcessStartInfo();


                    var server = new HttpServerUtilityWrapper(HttpContext.Current.Server);
                    string file = server.MapPath("~/Content/Application/growble.server.start.exe");

                    ps.FileName = file;
                    ps.Arguments = name;

                    try
                    { //Start the server
                    Process.Start(ps);}
                    
                    catch(Exception ex)
                    {
                        this.Clients.Caller.connectToWorld(ex.ToString());

                    }
                    if((activeWorld = wrep.IsWorldActive(name)) != null)
                    {
                        //Give the player the connection details
                        this.Clients.Caller.connectToWorld(activeWorld.Connection);
                        
                    }
                    
                    
                }
                catch(Exception ex)
                {

                    this.Clients.Caller.connectToWorld("Epic Fails");
                }
            }
        }

        public void NewMethod()
        {

        }

        public void ServerChat(string message)
        {

        }

        public override System.Threading.Tasks.Task OnDisconnected(bool dtrue)
        {

            AccountRepo arep = new AccountRepo();
            UserRepo urep = new UserRepo();

            urep.Loggout(new sim.Data.OnlineUser() { Name = this.Context.User.Identity.Name });

            return base.OnDisconnected(dtrue);
        }
    }
}