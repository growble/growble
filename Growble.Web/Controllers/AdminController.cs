﻿using growble.sim.Data;
using growble.sim.Data.Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace growble.Controllers
{
    public class AdminController : Controller
    {
        //
        // GET: /Admin/

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        [ValidateInput(false)] 
        public ActionResult Add(Article article)
        {
            WebRepo wr = new WebRepo();
            wr.AddArticle(article);
            return RedirectToAction("Index", "Admin");
        }


        [HttpGet]
        public ActionResult Edit(string id)
        {
            WebRepo wr = new WebRepo();
            return View(wr.GetArticle(id));
        }

        [HttpPost]
        [ValidateInput(false)] 
        public ActionResult Edit(Article article)
        {
            WebRepo wr = new WebRepo();
            wr.UpdateArticle(article);
            return RedirectToAction("Index", "Admin");
        }

    }
}
