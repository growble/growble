﻿


var registerState = {

    create: function()
    {

        $('#game-canvas-gui-forms').html('');

        $('<div class="login-option">Username</div>').appendTo($('#game-canvas-gui'));
        $('<div class="login-option"><input id="register-username" type="text"/></div>').appendTo($('#game-canvas-gui'));
        $('<div class="login-option">Password</div>').appendTo($('#game-canvas-gui'));
        $('<div class="login-option"><input id="register-password" type="password"/></div>').appendTo($('#game-canvas-gui'));
        var submit = $('<div class="login-option"><submit>Register</submit></div>').appendTo($('#game-canvas-gui'));
        $('<div class="login-option"><p id="register-error"></p></div>').appendTo($('#game-canvas-gui'));
       
        submit.click(function () {

            core.server.register($('#register-username').val(), $('#register-password').val());

        });

       
    }
}




core.on("usernameUsed", function () {
    $('#login-error').html("That Username is either invalid or allready in use, only A-Z chars can be used");

});
