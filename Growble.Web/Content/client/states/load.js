﻿var start = "../";
var loadState = {

    preload: function()
    {
        //game.load.image("Water", start + '/Content/assets/tiles/water.png');
        game.load.image("Air",start + '/Content/assets/tiles/Grass.png');
        game.load.image("Grass", start + '/Content/assets/tiles/Grass.png');
        game.load.image("WorldDoor", start + '/Content/assets/tiles/WorldDoor.png');
        game.load.image("Door", start + '/Content/assets/tiles/Door.png');
        game.load.image("BedRock", start + '/Content/assets/tiles/BedRock.png');
        game.load.image("DeathSpikes", start + '/Content/assets/sprites/deathspikes.png');
        game.load.image("Ice", start + '/Content/assets/sprites/ice.png');
        game.load.image("Brick", start + '/Content/assets/sprites/bricks.png');
        game.load.image("Lava", start + '/Content/assets/sprites/lava.png');
        game.load.image("FloorBoards", start + '/Content/assets/sprites/woodpanel.png');
        game.load.image("DaVinci", start + '/Content/assets/sprites/davinci.png');
        game.load.image("SpritePlayer", start + '/Content/assets/sprites/player.png');
        game.load.image("SpritePlayerArm", start + '/Content/assets/sprites/body/playerArm.png');
        game.load.image("SpritePlayerFoot", start + '/Content/assets/sprites/body/playerFoot.png');
        game.load.image("SpritePicker", start + '/Content/assets/sprites/picker.png');
        game.load.image("SpritePuff", start + '/Content/assets/sprites/puff.png');
        game.load.image("Hammer", start + '/Content/assets/sprites/hammer.png');
        game.load.image("ScrewDriver", start + '/Content/assets/sprites/screwdriver.png');
        game.load.image("Hammer2", start + '/Content/bigimage.png');
        game.load.image("BrainHelmet", start + '/Content/assets/sprites/clothes/brain-hat.png');
        game.load.image("BrainBody", start + '/Content/assets/sprites/clothes/brain-body.png');
        game.load.image("Suit", start + '/Content/assets/sprites/clothes/suit.png');
        game.load.spritesheet('Water', '/Content/assets/tiles/water.png', 32, 32);
    },
    create: function()
    {

        $('<div class="load-bar"></div>').appendTo($('#game-canvas-gui'));


    },
    loadUpdate: function (progress) {

        $('.load-bar').width(game.load.progress + ' %');
        console.log(game.load.progress)

        if (game.load.progress > 99)
        {

            game.state.add('login', loginState);
            game.state.start('login');
        }
    }
}
