﻿var worldconn = null;

var PlayersList = {};

var worldsState = {

    create: function () {

        $('#game-canvas-gui-forms').html('');

        var worldSelect = $('<div class="world-input"></div>').appendTo($('#game-canvas-gui-forms'));
        var worldinput = $('<input/>').appendTo(worldSelect);
        var go = $('<submit>Go!</submit>').appendTo(worldSelect);

        core.invoke("getWorlds");

        ///Display to user
        core.on("startWorlds", function (worlds) {

            var fontsize = 60;

            for (x in worlds)
            {
                 var button = $('<div class="start-world" style="font-size:' + fontsize + 'px">' + worlds[x] + '</div>').appendTo($('#game-canvas-gui-forms'));
                 button.click(function () {

                     core.invoke("joinWorld", this.valueOf());
                     console.log(this.valueOf());
                 }.bind(worlds[x]));

                 fontsize = fontsize - 1;
            }
        });

        go.click(function () {

            console.log(worldinput.val());
            core.invoke("joinWorld", worldinput.val());
        });

    },

}

/// When Connected to a world
///     Define Connection Callbacks
///
///
core.on("connectToWorld", function (connString) {

    var worldConnection = $.hubConnection(connString);


    worldHub = worldConnection.createHubProxy('worldHub');


    worldHub.on("setWorld", function (sworld, splayers, splayer) {

        ActiveWorld = new GameWorld();

        ActiveWorld.InitWorld(sworld, splayers, splayer);

        game.state.add('worldPlay', ActiveWorld);
        game.state.start('worldPlay');

    });

    worldHub.on("playerAdded", function (player) {

        //PlayersList[player.Name] = player;

    });

    worldHub.on("updatePlayerPositions", function (players) {

        ActiveWorld.playerPositionsUpdate(players);
    });

    worldHub.on("SetGems", function (gems) {

        $('#gemcount').html(gems);

        $('#gemcount').effect("shake");
    });


    worldHub.on("blockSet", function (x, y, name) {

        ActiveWorld.blockSet(x, y, name);

    });

    worldHub.on("blockDestroyed", function (x,y) {

        ActiveWorld.blockDestroyed(x, y);
    });
    
    worldHub.on("leaveWorld", function () {

        location.reload(); 
    });

    worldHub.connection.start().done(function () {


        console.log("Connected to World");
        

        worldHub.invoke("Authenticate", servername);
    }).done(function () {

        console.log("Connected to Server Core");

    }).fail(function (error) {
        console.log(error);
    });


});

