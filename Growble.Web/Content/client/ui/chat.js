﻿
var InfoVisible = false;
///Creates the chat bar
var startChat = function()
{
    return;

    $('#game-canvas-chat').html("");

    var chatDiv = $('<div class="chat"></div>').appendTo($('#game-canvas-gui'));

    var optionsDiv = $('<div class="chatchooser"></div>').appendTo(chatDiv);
    var globalChat = $('<div>Global</div>').appendTo(optionsDiv);
    var worldChat = $('<div>World</div>').appendTo(optionsDiv);
    var inventory = $('<div>Inventory</div>').appendTo(optionsDiv);
    var inventory = $('<div>Menu</div>').appendTo(optionsDiv);
    var minimize = $('<div>Minimize!</div>').appendTo(optionsDiv);

    InfoVisible = true;

    //Minimize
    minimize.click(function () {

        if (InfoVisible)
        {

            chatDiv.height(0);
            InfoVisible = false;
            minimize.html("Maximize!")
        }
        else
        {
            InfoVisible = true;
            chatDiv.height(200);
            minimize.html("Minimize!")
        }
    })


    ///GLOBAL



    ///WORLD HUB
    var chinput = $('<input value="Type Here and press enter to chat..."/>').appendTo(chatDiv);

    $('<submit></submit>').appendTo(chatDiv);

    chinput.click(function () {

        if (chinput.val() == "Type Here and press enter to chat...")
        {
            chinput.val("");
        }
    });

    $(chinput).keypress(function (e) {
        if (e.which == 13) {

            worldHub.invoke("Chat", chinput.val());
            chinput.val("");
        }
    });

    var messages = $('<ul></ul>').appendTo(chatDiv);

    chatDiv.hover(function (ev) {
        disableKeyboard();
    },
    function (ev) {
        initKeyboard();
    });

    worldHub.on("worldChat", function (user, message) {

        $('<li> ' + user + ' : ' + message + ' </li>').prependTo(messages);

        $('<div class="game-chat">' + message+ '</div>').appendTo($('#game-canvas-gui'));

    });

};


