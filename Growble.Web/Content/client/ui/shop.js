﻿ 
function Shop()
{
    this.ShopDiv = $('<div class="shop"></div>').appendTo($('#game-canvas-gui'));


    $('<h1>Gem Store!</h1>').appendTo(this.ShopDiv);
    $('<p>Spend youre hard earned gems on some funky stuff</p>').appendTo(this.ShopDiv);
    this.BuyGemsButton = $('<a>Get Some More Gems ASAP.</a>').appendTo(this.ShopDiv);
    this.CloseShop = $('<a>Close Shop</a>').appendTo(this.ShopDiv);


    this.BuyGemsButton.click(function () {
        this.buyGems();
    }.bind(this));

    this.ShopDiv.hide();

    this.CloseShop.click(function () {

        this.ShopDiv.hide();
    }.bind(this));

    worldHub.on("initCatalog", function (cat) {

        this.initCatalog(cat);
    }.bind(this));
}

Shop.prototype.buyGems = function()
{
    $('<iframe src="https://www.superrewards-offers.com/super/offers?h=ifhhlxfukdh.50954434497&hoffers=1&uid=USER_ID" frameborder="0" width="880" height="400" scrolling="no"></iframe>')
    .appendTo(this.ShopDiv);
}


Shop.prototype.show = function () {

    this.ShopDiv.show(2000);

}

Shop.prototype.initCatalog = function (cat) {

    $('<p>All the items!</p>').appendTo(this.ShopDiv);

    var catalogDiv = $('<div></div>').appendTo(this.ShopDiv);
    for (var i in cat)
    {

        var shopitem = $('<div class="shop-item"></div>').appendTo(catalogDiv)
        //$('<p>' + cat[i].Name + ' </p>').appendTo(shopitem);

        console.log(game.cache.getImage(cat[i].Name));

        $(game.cache.getImage(cat[i].Name)).clone().appendTo(shopitem);

        $('<p> ' + cat[i].Cost + '</p>').appendTo(shopitem);
        var buyButton = $('<a> Buy </a>').appendTo(shopitem);


        //Buy Button
        buyButton.click(function () {
            worldHub.invoke("BuyItem",this, 1);
        }.bind(cat[i].Name));
    }



}
