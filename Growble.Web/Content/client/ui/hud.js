﻿
function HUD()
{
    this.Panel = $('<div class="chat"></div>').appendTo($('#game-canvas-gui'));;

    this.GemDiv = null;
    this.GemCount = null;

    this.ShopDisplay = new Shop();

    //Tabs
    this.Tabs = $('<div class="chatchooser"></div>').appendTo(this.Panel);
    this.GlobalButton = $('<div>Global</div>').appendTo(this.Tabs);
    this.WorldButton = $('<div>World</div>').appendTo(this.Tabs);
    this.InventoryButton = $('<div>Inventory</div>').appendTo(this.Tabs);
    this.MenuButton = $('<div>Menu</div>').appendTo(this.Tabs);
    this.MinButton = $('<div>Minimize!</div>').appendTo(this.Tabs);


    this.Visible = false;

    this.GlobalDiv = null;
    this.WorldDiv = null;
    this.InventoryDiv = null;
    this.MenuDiv = null;
    
    //Init the HUD
    this.createHUD();
}

HUD.prototype.createHUD = function () {

    //If User Hovers on the div, make sure we enable/disable the keyboard input as appropriate
    this.Panel.hover(function (ev) {
        disableKeyboard();
    },
    function (ev) {
        initKeyboard();
    });

    //Create Tabs
    this.createGems();
    this.createWorldChat();
    this.createInventory();
    this.createMenu();


    this.Visible = false;
    this.Panel.height(0);

    //Minimize
    this.MinButton.click(function () {

        if (this.Visible) {

            this.Panel.height(0);
            this.Visible = false;
            this.closeAll();
        }
        else {
            this.Visible = true;
            this.Panel.height(200);
            this.closeAll();

        }
    }.bind(this))

    //Buttons
    this.WorldButton.click(function () {
        console.log("!");
        this.closeAll();
        this.WorldDiv.show();

        if (! this.Visible)
        {

            this.Panel.height(200);
            this.Visible = true;
        }

    }.bind(this));

    this.InventoryButton.click(function () {
        console.log("!");
        this.closeAll();
        this.InventoryDiv.show();


        if (!this.Visible) {

            this.Panel.height(200);
            this.Visible = true;
        }
    }.bind(this));

    this.MenuButton.click(function () {

        console.log("!");
        this.closeAll();
        this.MenuDiv.show();


        if (!this.Visible) {

            this.Panel.height(200);
            this.Visible = true;
        }

    });


    worldHub.on("errorMessage", function (text) {
        this.addError(text);
    }.bind(this));


    worldHub.on("infoMessage", function (text) {
        this.addInfo(text);
    }.bind(this));



    ///Gems
    this.GemDiv = $('<div class="hud-gems"></div>').appendTo($('#game-canvas-gui'));

    ///Gems
    var heartsdiv = $('<div class="hud-life"></div>').appendTo($('#game-canvas-gui'));
    $('<img src="../Content/assets/sprites/heart.png"/>').appendTo(heartsdiv);
    $('<p id="hud-life-points">15</p>').appendTo(heartsdiv);


    worldHub.on("updateLife", function (life) {

        $('#hud-life-points').html(life);
        console.log("this is dave");




    });

    worldHub.on("doorDetails", this.doorDetailsUpdate);

};

HUD.prototype.createGems = function () {

    ///Gems
    this.GemDiv = $('<div class="hud-gems"></div>').appendTo($('#game-canvas-gui'));

    $('<img src="../Content/assets/sprites/gem.png"/>').appendTo(this.GemDiv);
    this.GemCount = $('<p id="gemcount">200</p>').appendTo(this.GemDiv);


    this.GemDiv.click(function () {
        this.ShopDisplay.show();
    }.bind(this));
};

HUD.prototype.createGlobalChat = function () {

    this.GlobalDiv = $('<div></div>').appendTo(this.Panel);
    this.GlobalDiv.hide();

};

HUD.prototype.createWorldChat = function () {


    this.WorldDiv = $('<div></div>').appendTo(this.Panel);
    this.WorldDiv.hide();
    

    ///WORLD HUB
    var chinput = $('<input value="Type Here and press enter to chat..."/>').appendTo(this.WorldDiv);

    $('<submit></submit>').appendTo(this.WorldDiv);

    chinput.click(function () {

        if (chinput.val() == "Type Here and press enter to chat...")
        {
            chinput.val("");
        }
    });

    $(chinput).keypress(function (e) {
        if (e.which == 13) {

            worldHub.invoke("Chat", chinput.val());
            chinput.val("");
        }
    });

    var messages = $('<ul></ul>').appendTo(this.WorldDiv);


    worldHub.on("worldChat", function (user, message) {

        $('<li> ' + user + ' : ' + message + ' </li>').prependTo(messages);




    });



    worldHub.on("updateLife", function (life) {

        $('#hud-life-points').html(life);
        console.log("this is dave");




    });


};

HUD.prototype.createMenu = function () {

    this.MenuDiv = $('<div id="dmenu"></div>').appendTo(this.Panel);
    this.MenuDiv.hide();

    $('<span class="menu-option">Leave World!</span>').appendTo(this.MenuDiv);
};

HUD.prototype.createInventory = function () {

    this.InventoryDiv = $('<div></div>').appendTo(this.Panel);
    this.InventoryDiv.hide();

    for (var i = 0; i < 45; i++)
    {
        var item = $('<div id="inventory-' + i + '" class="inventory-item"><img/><p>0</p></div>').appendTo(this.InventoryDiv);

        item.click(function () {

            if ($('#inventory-' + this).hasClass('item-equipped'))
            {
                worldHub.invoke("DequipItem", this);
            }
            else
            {

                worldHub.invoke("EquipItem", this);
            }

        }.bind(i));
    }


    //When the Server sends an inventory update
    worldHub.on("setInventory", function (inventory) {

        console.log(inventory);

        for (var i in inventory.Items)
        {
            $('#inventory-' + i ).html(game.cache.getImage(inventory.Items[i].Item.Name));

            $('<p>' + inventory.Items[i].Quantity +'</p>').appendTo($('#inventory-' + i));
            if (inventory.Items[i].Equipped)
            {
                console.log("It's Equipped!");
                $('#inventory-' + i ).addClass("item-equipped");
            }
            else
            {
                console.log("It's Dequipped!");
                $('#inventory-' + i ).removeClass("item-equipped");
            }

        }

    });
};

HUD.prototype.setInventory = function (inventory) {

    for (var item in inventory)
    {

    }
};

HUD.prototype.closeAll = function () {

    this.InventoryDiv.hide();
    this.WorldDiv.hide();
};

var lastTimeoutHUD = null;
var lastTimeoutHUDerr = null;

HUD.prototype.addError = function (text) {

    if (lastTimeoutHUDerr != null)
    {
        clearTimeout(lastTimeoutHUDerr);
        $('.error').remove();
    }

    $('<div class="error">' + text + '<div>').appendTo($('#game-canvas-gui'));
    

    lastTimeoutHUDerr = setTimeout(function () {
        $('.error').remove();
    }, 4000);
};

HUD.prototype.addInfo = function (text) {

    if (lastTimeoutHUD != null) {
        clearTimeout(lastTimeoutHUD);
        $('.info').remove();
    }

    $('<div class="info">' + text + '<div>').appendTo($('#game-canvas-gui'));


    lastTimeoutHUD = setTimeout(function () {
        $('.info').remove();
    }, 4000);
};

HUD.prototype.doorDetailsUpdate = function (id, world) {

    var updateDoors = $('<div class="infoform"> <h2>Door ' + id + ' settings.</h2> <label>Goes to</label> <input type="text" /> <button id="pressok" class="infoform-ok">Ok</button> <button class="infoform-cancel">Cancel</button> </div>').appendTo($("#game-canvas-gui"));

    $('.infoform-ok').click(function () {

    });
    $('.infoform-cancel').click(function () {
        $('.infoform').remove();
    });

    updateDoors.hover(function (ev) {
        disableKeyboard();
    },
    function (ev) {
        initKeyboard();
    });

};