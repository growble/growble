﻿
function Player (name, x, y)
{
    this.Name = name;
    this.Chat = null;
    this.ItemSprites = null;
    this.NameText = null;
    this.X = x;
    this.Y = y;

    this.Sprite =  new Phaser.Sprite(game, x * global.scale, y * global.scale, "SpritePlayer");
    this.Sprite.scale.x = global.mscale ;
    this.Sprite.scale.y = global.mscale ;

    this.Sprite.z = global.mscale0;

    this.LocationTween = null;
    this.TextTween = null;
    this.NameTween = null;

    this.PlayerSpriteGroup = new Phaser.Group(game, this.Sprite);
    this.PlayerSpriteGroup.z = 7;
    //Add Arms
    this.ArmSpriteLeft = new Phaser.Sprite(game, 16, 14, "SpritePlayerArm");
    this.ArmSpriteRight = new Phaser.Sprite(game, 16, 14, "SpritePlayerArm");

    this.HandSpriteGroup = null;

    //this.FootSpriteLeft = new Phaser.Sprite(game, 11, 25, "SpritePlayerFoot");
    //this.FootSpriteRight = new Phaser.Sprite(game, 18, 25, "SpritePlayerFoot");

    this.ArmSpriteLeftTween = game.add.tween(this.ArmSpriteLeft).to(
        { angle: 60 },
        300,
     Phaser.Easing.Linear.None, true, 0, Number.MAX_VALUE, true);

    this.ArmSpriteRightTween = game.add.tween(this.ArmSpriteRight).to(
        { angle: -60 },
        350,
     Phaser.Easing.Linear.None, true, 0, Number.MAX_VALUE, true);

    //Init Arms
    this.ArmSpriteLeftGroup = new Phaser.Group(game, this.ArmSpriteLeft);
    this.ArmSpriteLeftGroup.z = 6;
    this.ArmSpriteLeft.anchor.x = 0.5; 
    this.ArmSpriteLeft.scale.y = 1;
    this.ArmSpriteRight.scale.y = 1;
    this.ArmSpriteLeft.z = 5;
    this.ArmSpriteRight.z = 5;


   // this.PlayerSpriteGroup.add(this.FootSpriteLeft);
   // this.PlayerSpriteGroup.add(this.FootSpriteRight);
    this.PlayerSpriteGroup.add(this.ArmSpriteLeft);
    this.PlayerSpriteGroup.add(this.ArmSpriteRight);

    this.PlayerSpriteGroup.scale.set(1, 1);

    //Create Name Tag

    var style = { font: "25px Arial", fill: "#FF7F00", align: "center" };

    if (this.Chat != null)
        this.Chat.destroy(true);

    this.NameText = game.add.text(this.Sprite.x, this.Sprite.y, '@' + name, style);
    this.NameText.setShadow(5, 5, 'rgba(0,0,0,0.5)', 15);
    this.NameText.anchor.set(0.5);


    //Adds a speech bubble above the player - updated each frame 
    worldHub.on("worldChat", function (user, message) {

        if (user == this.Name)
        {

            console.log("1");
            var style = { font: "25px Arial", fill: "#ffffff", align: "center" };

            if (this.Chat != null)
                this.Chat.destroy(true);

            this.Chat = game.add.text(this.Sprite.x, this.Sprite.y, '"' + message + '"', style);
            this.Chat.setShadow(5, 5, 'rgba(0,0,0,0.5)', 15);
            this.Chat.anchor.set(0.5);

            game.add.tween(this.Chat).to({ alpha: 0 }, 6000, Phaser.Easing.Cubic.In, true);

        }

    }.bind(this));

    worldHub.on("playerInventoryUpdate", function (name, hand, clothing) {
        if (this.Name == name)
        {
            this.setInventory(hand, clothing);
        }

    }.bind(this));
}

Player.prototype.SetPosition = function (posx, posy) {


    this.X = posx;
    this.Y = posy;

    ///Add a short tween
    if (this.LocationTween) {
        game.tweens.remove(this.LocationTween);
    }

    this.LocationTween = game.add.tween(this.Sprite).to(
        { x: Math.floor(posx * global.scale), y: Math.floor(posy * global.scale) },
        180,
     Phaser.Easing.Linear.None, true);

    //Tween any chat
    if (this.Chat != null)
    {
        if (this.TextTween) {
            game.tweens.remove(this.TextTween);
        }

        this.TextTween = game.add.tween(this.Chat).to(
        { x: Math.floor(posx * global.scale) + 96/2, y: Math.floor(posy * global.scale) -26},
        180,
        Phaser.Easing.Linear.None, true);
    }

    if (this.NameTween)
    {
        game.tweens.remove(this.NameTween);
    }

    //Tween Name
    this.NameTween = game.add.tween(this.NameText).to(
    { x: Math.floor(posx * global.scale) +96/2, y: Math.floor(posy * global.scale) - 10 },
    180,
    Phaser.Easing.Linear.None, true);

    if (Math.abs(posx * global.scale - this.Sprite.position.x) < 10)
    {
        this.ArmSpriteRight.angle = -30;
        this.ArmSpriteLeft.angle = 30;
        this.ArmSpriteLeftTween.pause();
        this.ArmSpriteRightTween.pause();
    }
    else
    {

        this.ArmSpriteLeftTween.resume(); this.ArmSpriteRightTween.resume();
    }

    if ((this.Sprite.position.x - posx * global.scale) < -20) {
        this.Sprite.scale.x = -global.mscale;
        this.Sprite.anchor.x = 1;
        this.PlayerSpriteGroup.scale.set(1, 1);
        this.PlayerSpriteGroup.position.x = -32;


    }
    else if ((this.Sprite.position.x - posx * global.scale) > 20) {
        this.Sprite.scale.x = global.mscale;
        this.Sprite.anchor.x = 0;
        this.PlayerSpriteGroup.scale.set(1, 1);
        this.PlayerSpriteGroup.position.x = 0;

    }
    else
    {

        if (this.Chat != null)
        {
            
        }
    }
};

Player.prototype.setInventory = function (hand, clothing) {

    //Delete all items
    for (var i in this.ItemSprites)
    {
        this.PlayerSpriteGroup.remove(this.ItemSprites[i]);
        this.ItemSprites[i].destroy(true);
    }
    this.ItemSprites = {};

    //Add new clothing
    for (var i in clothing)
    {
        this.ItemSprites[i] = new Phaser.Sprite(game, 0, 0, clothing[i].Name);


        this.PlayerSpriteGroup.add(this.ItemSprites[i]);

    }


    this.setHand(hand);
};

Player.prototype.setHand = function (hand) {

    if (this.HandSprite != null)
    {
        this.HandSprite.destroy(true);
    }

    if (hand == null)
    {

    }
    else
    {

        this.HandSprite = new Phaser.Sprite(game, -1,17, hand.Name);
        this.HandSprite.scale.set(1 /4, 1 / 4);
        this.HandSprite.anchor.set(1, 1);
        this.HandSprite.angle = -30;

        this.HandSprite.z = this.HandSprite.z - 43;
        this.ArmSpriteLeftGroup.add(this.HandSprite);

        this.HandSprite.z = -1 + this.ArmSpriteLeft.z;
    }
};

Player.prototype.crayhands = function (x,y) {

    if (this.HandsTween)
    {
        game.tweens.remove(this.HandsTween);
    }

    this.HandsTween = game.add.tween(this.ArmSpriteLeft).to(
        { angle: 720},
        400,
     Phaser.Easing.Linear.None).start();

};