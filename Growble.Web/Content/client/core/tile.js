﻿

function Tile( x, y, type, emitter)
{
    this.X = x;
    this.Y = y;
    this.BlockType = type;
    //this.ParticleEmmiter = game.add.emitter(0, 0, 100);
    //this.ParticleEmmiter.z = 120;
    //this.ParticleEmmiter.width =128;
    //this.ParticleEmmiter.height = 128;

    ///Create the sprite
    this.Sprite = new Phaser.Sprite(game, x * global.scale, y * global.scale, type);
    this.Sprite.scale.x = global.mscale;
    this.Sprite.scale.y = global.mscale;

    this.Sprite.animations.add('walk', [0, 1, 2, 3]);
    this.Sprite.animations.play('walk', 2, true);
    this.Sprite.inputEnabled = true;
    this.Sprite.events.onInputDown.add(this.onClick.bind(this), game);
    this.Sprite.input.priorityID = 100;

    this.Sprite.input.pixelPerfectClick = true;
    //this.Sprite.autoCull = true;

    this.Sprite.visible = true;

}

//What the tile should do when clicked
Tile.prototype.onClick = function (pointer)
{
    ActiveWorld.Player.crayhands(this.X, this.Y);

    worldHub.invoke("Click", this.X, this.Y);

    console.log("clicky done");
}

