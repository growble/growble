﻿/// <reference path="player.js" />

function GameWorld()
{

    game.time.advancedTiming = true;

    this.Name = "";
    this.Players = {};

    //The World's Player
    this.Player = null;

    this.HUDisplay = null; //The Players HUD

    //Raw Server Data
    this.serverplayers = null;
    this.gameworld = null;
    this.currplayer = null;

    this.PlayerLayer = null;


    this.Tiles = {};
}

GameWorld.prototype.InitWorld = function (dworld, playersList, currentPlayerName) {

    // this.Clients.Caller.SetWorld(WorldData.World, WorldData.Players, this.GetPlayer().Name);

    //We have to wait until "create"
    this.gameworld = dworld;
    this.serverplayers = playersList;
    this.currplayer = currentPlayerName;
    this.HUDisplay = new HUD();


};

GameWorld.prototype.create = function () {

    //Core game atts
    $('#game-canvas-gui-forms').html("");
    game.world.setBounds(0, 0, global.scale * 100, global.scale * 50);
    game.stage.smoothed = false;

    
    //Create Tilemap
    this.TileLayer = game.add.group();
    this.TileLayer.z = 1;

    for (var x = 0; x < 100; x++)
        for (var y = 0; y < 50; y++) {

            this.blockSet(x, y, this.gameworld.Blocks[x][y].Name);
        }


    this.PlayerLayer = game.add.group();
    this.PlayerLayer.z = global.mscale;

    //Create Players
    for (var i in this.serverplayers) {
        this.Players[this.serverplayers[i].Name]
            = new Player(this.serverplayers[i].Name, this.serverplayers[i].X, this.serverplayers[i].Y);

        //Add to game
        this.PlayerLayer.add(this.Players[this.serverplayers[i].Name].Sprite)

        //Get our player
        if (this.serverplayers[i].Name == this.currplayer) {
            this.Player = this.Players[this.serverplayers[i].Name];
        }

        console.log(this.Players[this.serverplayers[i].Name]);
    }

    //Init Keyboard
    initKeyboard();
    resizeGame();

    //

    setInterval(function () { this.cullTiles() }.bind(this), 500);
};

GameWorld.prototype.update = function () {
  
    console.log(game.time.fps);

    //Make Sure we follow the player
    game.camera.follow(this.Player.Sprite);

    game.camera.screenView.width = 1;
    game.camera.screenView.height = 1;


};

GameWorld.prototype.render = function () {


};

GameWorld.prototype.playerPositionsUpdate = function (players) {
   

    for (var player in players)
    {
        this.Players[players[player].Name].SetPosition(
            players[player].X,
            players[player].Y
            );
    }

};

GameWorld.prototype.blockDestroyed = function (x, y) {

    //Remove the sprite
    this.Tiles[x][y].Sprite.destroy();
};

GameWorld.prototype.cullTiles = function () {
    
    if (this.Player != null)
        for (var x in this.Tiles)
        {
            for (var y in this.Tiles[x])
            {
                var tile = this.Tiles[x][y];
                var sprite = tile.Sprite;
                var Dx = Math.abs(tile.X - this.Player.X);
                var Dy = Math.abs(tile.Y - this.Player.Y);

                sprite.visible = false;
                if (Dx < 11)
                {
                    if (Dy < 6) {
                        sprite.visible = true;
                    }
                    else
                        sprite.visible = false;
                }
                    
                else
                {
                    sprite.visible = false;
                }
            }
        }

        
};

GameWorld.prototype.blockSet = function (x, y,name) {

    if (this.Tiles[x] == null)
        this.Tiles[x] = {};

    if (this.Tiles[x][y] != null)
    {
        //Remove the sprite
        this.Tiles[x][y].Sprite.destroy();
    }


    //Create new map
    var mapTile = new Tile(x, y, name);

    this.Tiles[x][y] = mapTile;

    this.TileLayer.add(mapTile.Sprite);


};