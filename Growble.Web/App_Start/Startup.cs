﻿using Owin;
using Microsoft.Owin;
using System;
using System.Threading;
using growble.Hubs;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System.Reflection;
using System.Collections.Generic;

[assembly: OwinStartup(typeof(SignalRChat.Startup))]
namespace SignalRChat
{
    public class Startup
    {
        public Startup()
        {

            var ass2 = AppDomain.CurrentDomain.GetAssemblies();
        }

        public void Configuration(IAppBuilder app)
        {

            
            app.MapSignalR(new Microsoft.AspNet.SignalR.HubConfiguration() { EnableJSONP = true});
        }
    }



}