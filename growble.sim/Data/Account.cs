﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Data
{
    public class Account
    {
        public Account()
        {
            this.PlayerInventory = new Inventory();
        }

        [BsonId(IdGenerator=typeof(ObjectIdGenerator))]
        public ObjectId Id { get; set; }

        public string Name { get; set; }
        public string Password { get; set; }
        public int Gems { get; set; }
        public string Email { get; set; }

        public List<Friend> Friends { get; set; }

        public Inventory PlayerInventory { get; set; }
    }
}
