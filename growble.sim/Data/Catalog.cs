﻿using growble.sim.Object.Blocks.Types;
using growble.sim.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Data
{
    public class Catalog
    {

        private List<IItem> items { get; set; }
        /// <summary>
        /// All the items in the catalog
        /// </summary>
        public List<IItem> Items
        {
            get
            {
                this.items = new List<IItem>();
                Assembly ass = typeof(IItem).Assembly;

                foreach (var item in ass.GetTypes())
                {
                    if (typeof(IItem).IsAssignableFrom(item) &&
                        typeof(IBuyable).IsAssignableFrom(item))
                    {
                        //Create the item
                        this.items.Add((IItem)Activator.CreateInstance(item));
                    }
                }

                return this.items;
            }
        }
    }
}
