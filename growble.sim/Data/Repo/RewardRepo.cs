﻿using MongoDB.Bson;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Data.Repo
{
    public class RewardRepo : IRepo
    {
        public RewardRepo() : base()
        {

        }

        public void AddReward(Reward r)
        {

            var rewards = this.Database.GetCollection<Reward>("rewards");

            rewards.Insert(r);
        }

        public List<Reward> GetRewards(string name)
        {

            var rewards = this.Database.GetCollection<Reward>("rewards");

            return rewards.Find(Query.EQ("User", name)).ToList();
        }

        public Reward DeleteAndReturnReward(string id)
        {

            var rewards = this.Database.GetCollection<Reward>("rewards");
            

            var reward = rewards.Find(Query.EQ("_id", new ObjectId(id))).SingleOrDefault();
            if (reward != null)
            {
                rewards.Remove(Query.EQ("_id", new ObjectId(id)));
                return reward;

            }
            else
                return null;
        }
    }
}
