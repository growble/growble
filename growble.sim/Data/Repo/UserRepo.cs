﻿using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Data.Repo
{
    public class UserRepo : IRepo
    {
       
        public void Loggin(OnlineUser user)
        {
            if (IsLoggedIn(user.Name))
            {
                //Dunno...
            }
            else
            {

                var accounts = this.Database.GetCollection<OnlineUser>("users");
                accounts.Insert(user);
            }
        }

        public bool IsLoggedIn(string name, out OnlineUser user)
        {

            var accounts = this.Database.GetCollection<OnlineUser>("users");

            var acc = accounts.Find(Query.EQ("Name", name)).FirstOrDefault();
            
            if (acc != null)
            {
                user = acc;
                return true;
            }
            else
            {
                user = new OnlineUser();
                return false;
            }

        }

        
        public bool IsLoggedIn(string name)
        {
            var accounts = this.Database.GetCollection<OnlineUser>("users");

            var acc = accounts.Find(Query.EQ("Name", name)).FirstOrDefault();

            if (acc != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
        public void Loggout(OnlineUser user)
        {

            var accounts = this.Database.GetCollection<OnlineUser>("users");
            
            accounts.Remove(Query.EQ("Name", user.Name));
        }

        public int PlayersLoggedIn()
        {
            var accounts = this.Database.GetCollection<OnlineUser>("users");
            return (int)accounts.Count();
        }

        public List<Friend> GetFriends(OnlineUser user)
        {
            AccountRepo arep = new AccountRepo();
            var acc =  arep.GetUser(user.Name);

            foreach (var x in acc.Friends)
            {
                x.Online = this.IsLoggedIn(x.Name); //Check if they are logged in.
            }

            return acc.Friends;
        }

        public void AddFriend(string user, Friend friend)
        {

            AccountRepo arep = new AccountRepo();
            var acc = arep.GetUser(user);
            acc.Friends.Add(friend);

            arep.SaveUser(acc);
        }
    }
}
