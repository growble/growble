﻿using MongoDB.Bson;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Data.Repo
{

    /// <summary>
    /// All the website content - articles etc.
    /// </summary>
    public class WebRepo : IRepo
    {
        public WebRepo() : base()
        {

        }

        public List<Article> GetAllArticles()
        {

            var articles = this.Database.GetCollection<Article>("articles");

            return articles.FindAll().ToList();
        }


        public List<Article> GetAllArticles(string cat)
        {

            var articles = this.Database.GetCollection<Article>("articles");

            return articles.Find(Query.EQ("Tag", cat)).ToList();
        }

        public Article GetArticle(string id)
        {
            ObjectId theid = new ObjectId(id);

            var articles = this.Database.GetCollection<Article>("articles");

            return articles.Find(Query.EQ("_id", theid)).FirstOrDefault();
        }

        public void AddArticle(Article article)
        {
            article.Date = DateTime.Now;
            
            var articles = this.Database.GetCollection<Article>("articles");

            articles.Insert(article); //add it
        }

        public void UpdateArticle(Article article)
        {

            var articles = this.Database.GetCollection<Article>("articles");

            articles.Update(Query.EQ("_id", article.Id), Update.Replace(article));
        }
    }
}
