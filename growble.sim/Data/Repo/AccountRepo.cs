﻿using growble.sim.Objects.Items.Tools;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Data.Repo
{
    public class AccountRepo : IRepo
    {
        public Account NewAccount(string name, string email, string password)
        {
            Account account = new Account();
            account.Name =name;
            account.Password = password;
            account.Email = email;
            account.Gems = 10000;
            account.PlayerInventory = new Inventory();
            account.PlayerInventory.Items = new List<InventoryItem>();
            account.PlayerInventory.Items.Add(new InventoryItem() { Item = new Hammer(), Quantity = 1 });
            account.PlayerInventory.Items.Add(new InventoryItem() { Item = new Screwdriver(), Quantity = 1 });

            var accounts = this.Database.GetCollection<Account>("accounts");

            accounts.Insert(account);

            return account;
        }

        public bool IsUsernameAvailable(string name)
        {

            var accounts = this.Database.GetCollection<Account>("accounts");

            var acc = accounts.Find(Query.EQ("Name", name)).Count();

            if (acc == 0)
                return true;
            else
                return false;
        }

        public Account IsValidLogin(string name, string password)
        {

            var accounts = this.Database.GetCollection<Account>("accounts");

            var acc = accounts.Find(Query.EQ("Name", name)).FirstOrDefault();

            if (acc != null && acc.Password == password)
                return acc;
            else
                return null;

        }

        public Account GetUser(String name)
        {

            var accounts = this.Database.GetCollection<Account>("accounts");

            var acc = accounts.Find(Query.EQ("Name", name)).FirstOrDefault();

            return acc;
        }

        public void SaveUser(Account player)
        {
            var accounts = this.Database.GetCollection<Account>("accounts");
            accounts.Update(Query.EQ("Name", player.Name), Update.Replace(player));
        }

        public static void GetData(string nopes)
        {

            var results = "Players"[0];




        }

        public int NumberOfUsers()
        {

            var accounts = this.Database.GetCollection<Account>("accounts");

            return (int)accounts.Count();
        }
    }
}
