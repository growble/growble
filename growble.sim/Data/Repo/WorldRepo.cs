﻿using growble.sim.Object;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Data.Repo
{
    public class WorldRepo : IRepo
    {
        public void StartWorld(OnlineWorld world)
        {

            var worlds = this.Database.GetCollection<OnlineWorld>("onlineworlds");

            if (this.IsWorldActive(world.Name) != null)
            {
                //
            }
            else
            {
                worlds.Insert(world); 
            }
        }

        public void EndWorld(OnlineWorld world)
        {


            //remove from the online worlds
            var worlds = this.Database.GetCollection<OnlineWorld>("onlineworlds");

            worlds.Remove(Query.EQ("Name", world.Name)); //Remove the world
        }

        public OnlineWorld IsWorldActive(string name)
        {
            var worlds = this.Database.GetCollection<OnlineWorld>("onlineworlds");
            var world = worlds.Find(Query.EQ("Name", name)).FirstOrDefault();
            return world;
        }
        
        public bool WorldExists(string name)
        {
            var worlds = this.Database.GetCollection<World>("worlds");

            if (worlds.Find(Query.EQ("Name", name)).Count() == 0)
                return false;
            else
                return true;
        }

        public void CreateWorld(string name)
        {
            World world = new World();
            world.Name = name;

            //We want the default world
            world.WorldObject = WorldObject.DefaultWorld;

            var worlds = this.Database.GetCollection<World>("worlds");
            worlds.Insert(world);
        }

        public World GetWorld(String name)
        {
            var worlds = this.Database.GetCollection<World>("worlds");
            return worlds.Find(Query.EQ("Name", name)).FirstOrDefault();
        }

        public void UpdateWorld(World world)
        {

            var worlds = this.Database.GetCollection<World>("worlds");
            worlds.Update(Query.EQ("Name", world.Name), Update.Replace(world)); //Update the world
        }

        /// <summary>
        /// Get a list of the most popular worlds
        /// </summary>
        /// <returns></returns>
        public List<string> StartWorlds()
        {

            var worlds = this.Database.GetCollection<OnlineWorld>("onlineworlds").FindAll();

            List<string> worldnames = new List<string>();

            worldnames.Add("start");
            worldnames.Add("tutorial");

            foreach (var x in worlds)
                worldnames.Add(x.Name);

            return worldnames;
        }
    }
}
