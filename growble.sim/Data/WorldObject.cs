﻿using growble.sim.Object.Blocks;
using growble.sim.Objects;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Object
{
    public class WorldObject
    {
        public string Name { get; set; }

        /// <summary>
        /// Which User owns this world?
        /// </summary>
        public string User { get; set; }

        public IBlock[,] Blocks { get; set; }

        /// <summary>
        /// Keeping a list of locks
        /// </summary>
        public List<Tile> Locks { get; set; }

        public WorldObject()
        {
            Blocks = new IBlock[200, 80];

            for (int x = 0; x < 200; x++)
                for (int y = 0; y < 80; y++)
                {

                }

            this.User = "";
            this.Locks = new List<Tile>();
        }

        public static WorldObject DefaultWorld 
        {
            get
            {
                WorldObject world = new WorldObject();
                world.Name = "DEFAULTNAME";
                world.Blocks = new IBlock[100,50];

                Random rand = new Random();

                for (int x = 0; x < 100; x++)
                {

                    for (int y = 0; y < 50; y++)
                    {
                        
                        world.Blocks[x, y] = new GrassBlock();
                        
                    }
                }

                world.Blocks[30, 30] = new WorldDoorBlock();
                world.Blocks[30, 31] = new BedrockBlock();

                return world;
            }
            set
            {
            }

        }

        public List<Tile> GetAboutPoint(int x, int y)
        {
            int delta = 1;

            List<Tile> Tiles = new List<Tile>();
            for (var X = x - delta; X < x + delta +1; X++)
                for (var Y = y - delta; Y < y + delta +1; Y++)
                {
                    if (X < 0 || Y < 0 || X > 99 || y > 49)
                    {
                        Tiles.Add(new Tile() { X = X, Y = Y, Block = new BedrockBlock() });
                    }
                    else
                    {
                        Tiles.Add(new Tile() { X = X, Y = Y, Block = this.Blocks[X, Y] });
                    }
                }

            return Tiles;
        }

        public List<Tile> GetWithin(RectangleF rect)
        {
            List<Tile> tiles = new List<Tile>();
            tiles.Add(GetTile((int)rect.X, (int)rect.Y));
            tiles.Add(GetTile((int)(rect.X + rect.Width), (int)rect.Y));


            tiles.Add(GetTile((int)rect.X, (int)(rect.Y + rect.Height)));
            tiles.Add(GetTile((int)(rect.X + rect.Width), (int)(rect.Y + rect.Height)));

            return tiles;
        }

        public Tile GetTile(int X, int Y)
        {
            if (X < 0 || Y < 0 || X > 99 || Y > 49)
            {
                return (new Tile() { X = X, Y = Y, Block = new BedrockBlock() });
            }
            else
            {
                return (new Tile() { X = X, Y = Y, Block = this.Blocks[X, Y] });
            }

        }

        public bool SetTile(int X, int Y, IBlock block)
        {
            if (X < 0 || Y < 0 || X > 99 || Y > 49)
            {
                return false;
            }
            else
            {
                //Set the blcok
                this.Blocks[X, Y] = block;
                return true;
            }

        }
    }
}
