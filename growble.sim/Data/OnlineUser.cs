﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Data
{
    public class OnlineUser
    {
        [BsonId(IdGenerator = typeof(ObjectIdGenerator))]
        public ObjectId Id { get; set; }

        public string Name { get; set; }
        public string ConnectionId { get; set; }

        public List<Friend> Firends { get; set; }
    }

    public class Friend
    {
        public string Name { get; set; }
        public bool Online { get; set; }
    }
}
