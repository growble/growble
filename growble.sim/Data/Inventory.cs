﻿using growble.sim.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim
{
    public class Inventory
    {

        public const int InventoryMax = 200;

        public Inventory()
        {
            this.Items = new List<InventoryItem>();
        }
        
        public List<InventoryItem> Items { get; set; }

        /// <summary>
        /// What's equipped hand
        /// </summary>
        public IHandEquipable Hand { get; set; }

        public List<IClothing> Clothing
        {
            get
            {
                var items = Items.Where(p => p.Item is IClothing && p.Equipped);

                var IItems = new List<IClothing>();
                foreach (var x in items)
                    IItems.Add(x.Item as IClothing);

                return IItems;
            }

        }

        public void SetEquipped(int i)
        {

            if (Items.Count > i)
            {
                //We've got the item
                var item = Items[i];

                if (item.Item is IHandEquipable)
                {
                    //Set all HandEquipable as unequiped...
                    foreach (var x in this.Items)
                    {
                        if (x.Item is IHandEquipable)
                            x.Equipped = false;
                    }

                    //Set this one as equipeed.
                    this.Hand = (IHandEquipable)item.Item;
                    item.Equipped = true;
                }
                else
                {
                    item.Equipped = true;
                }
            }
        }

        public void Dequip(int i)
        {
            if (Items.Count > i)
            {
                //We've got the item
                var item = Items[i];

                if (item.Item is IHandEquipable)
                    this.Hand = null;

                item.Equipped = false;
            }
        }

        public bool AddItem(IItem item, int q)
        {
            for (int i = 0; i < Items.Count; i++)
            {
                if (Items[i].Item.Name == item.Name)
                {
                    if (Items[i].Quantity + q > Inventory.InventoryMax )
                    {
                        return false;
                    }
                    else
                    {
                        Items[i].Quantity += q;
                        return true;
                    }
                }
            }

            Items.Add(new InventoryItem() { Item = item, Quantity = q, Equipped = false });

            return false;
        }

        public bool RemoveItem(int id, int quantity)
        {
            if (Items.Count > id)
            {
                //We've got the itdem
                var item = Items[id];

                    for (int i = 0; i < Items.Count; i++)
                    {
                        if (Items[i].Item.Name == item.Item.Name)
                        {
                            if (Items[i].Quantity - quantity < 0)
                            {
                                return false;
                            }
                            else
                            {
                                Items[i].Quantity -= quantity;
                                return true;
                            }
                        }
                    }

                return false;
            }

            return false;
            
        }

        /// <summary>
        /// Remove 1 of an item - if the player is holding it - we need
        /// to check and remove it from the equipped hand.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool RemoveItem(string name)
        {
            var count = Items.Where(p => p.Item.Name == name);

            if (count.Count() == 0)
            {
                Console.Out.WriteLine("Couldn't find any..." + name);
                return false;
            }
            else
            {
                var item = count.FirstOrDefault();
                if (item.Quantity == 0)
                {
                    this.Items.Remove(item);

                    //Remove it from the hand!
                    if ((this.Hand as IItem).Name == name)
                    {
                        this.Hand = null;
                    }

                    return false;
                }
                else if (item.Quantity == 1)
                {
                    this.Items.Remove(item);

                    if ((this.Hand as IItem).Name == name)
                    {
                        this.Hand = null;
                    }

                    return true;
                }
                else
                {
                    //Deduct one!
                    item.Quantity--;
                    return true;
                }
            }
        }
    }
}
