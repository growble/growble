﻿using growble.sim.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim
{
    public class InventoryItem
    {
        public IItem Item { get; set; }

        public int Quantity { get; set; }

        //Is this Item Equipped?
        public bool Equipped { get; set; }


    }
}
