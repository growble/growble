﻿using growble.sim.Object;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Data
{
    public class World
    {

        [BsonId(IdGenerator = typeof(ObjectIdGenerator))]
        public ObjectId Id { get; set; }

        /// <summary>
        /// THe name of the world
        /// </summary>
        public string Name { get; set; }

        
        public WorldObject WorldObject { get; set; }
        
    }
}
