﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Objects
{
    public class ItemDropList
    {
        /// <summary>
        /// The Item to drop,
        /// Value: The average number that should be dropped.
        /// </summary>
        public Dictionary<IItem, double> Drops { get; set; }
    }
}
