﻿using growble.sim.Object.Blocks.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Objects.Items.Tools
{
    /// <summary>
    /// It's a hammer!
    /// </summary>
    public class Hammer : IItem, IBuyable, IHandEquipable
    {
        /// <summary>
        /// It's a hammer!
        /// </summary>
        public string Name
        {
            get
            {
                return "Hammer";
            }
            set
            {
            }
        }

        public int Cost
        {
            get
            {
                return 120;
            }
            set
            {
            }
        }

        public string ShopDescription
        {
            get
            {
                return "Hammers, it is what it says on the hammer.";
            }
            set
            {
            }
        }
    }
}
