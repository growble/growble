﻿using growble.sim.Object.Blocks.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Objects.Items.Tools
{
    /// <summary>
    /// It's a hammer!
    /// </summary>
    public class Screwdriver : IItem, IBuyable, IHandEquipable
    {
        /// <summary>
        /// It's a hammer!
        /// </summary>
        public  string Name
        {
            get
            {
                return "ScrewDriver";
            }
            set
            {
            }
        }

        public int Cost
        {
            get
            {
                return 0;
            }
            set
            {
            }
        }

        public string ShopDescription
        {
            get
            {
                return "A screwdriver fixes things";
            }
            set
            {
            }
        }
    }
}
