﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Objects
{
    public interface IEquipable
    {
    }

    public interface IHandEquipable : IEquipable
    {

    }

    public interface IClothing : IEquipable
    {
        /// <summary>
        /// x - in pixels - offset
        /// </summary>
        int dx { get; set; }

        /// <summary>
        /// y - in pixels - offset
        /// </summary>
        int dy { get; set; }

        /// <summary>
        /// The Z Level of the clothing
        /// </summary>
        int z { get; set; }
    }
}
