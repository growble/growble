﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Objects
{
    interface IDoor
    {
        /// <summary>
        /// The Id of the door...
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// The world string go to door 4 in world "anthony" is "anthony.4"
        /// </summary>
        string World { get; set; }
    }
}
