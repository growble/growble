﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Object.Blocks.Types
{
    /// <summary>
    /// Is this block buyable in store?
    /// </summary>
    public interface IFriction
    {

        /// <summary>
        /// If 0 - 1 : 1 is no friction, 0 is absoulte friction (stops immediatly when force applied)
        /// 
        /// If < 0; Abs(value) = % of max speed..
        /// </summary>
        double Frictional { get; set; }
    }
}
