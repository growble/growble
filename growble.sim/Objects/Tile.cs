﻿using growble.sim.Object.Blocks;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Objects
{
    [BsonIgnoreExtraElements]
    public class Tile
    {
        public IBlock Block { get; set; }
        public int X { get; set; }
        public int Y { get; set; }

        [BsonIgnore]
        public RectangleF Rectangle
        {
            get
            {
                var rect = new RectangleF(X, Y, 1, 1);
                return rect;
            }
            set { }
        }
    }
}
