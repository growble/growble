﻿using growble.sim.Objects;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Object.Blocks
{
    /// <summary>
    /// Core Block Class
    /// </summary>
    public interface IBouncey
    {
        double Bounce { get; set; }
    }
}
