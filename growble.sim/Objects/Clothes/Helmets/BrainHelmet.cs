﻿using growble.sim.Object.Blocks.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Objects.Clothes.Helmets
{
    class Suit : IClothing, IItem, IBuyable
    {
        public int dx
        {
            get
            {
                return 0;
            }
            set
            {
            }
        }

        public int dy
        {
            get
            {
                return 0;
            }
            set
            {
            }
        }

        public int z
        {
            get
            {
                return 2;
            }
            set
            {
            }
        }

        public string Name
        {
            get
            {
                return "BrainHelmet";
            }
            set
            {
            }
        }

        public int Cost
        {
            get
            {
                return 100;

            }
            set
            {
            }
        }

        public string ShopDescription
        {
            get
            {
                return "A brain helmet - it's pretty cool";
            }
            set
            {
            }
        }
    }
}
