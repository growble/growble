﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Object.Blocks.Types
{
    /// <summary>
    /// Is this block buyable in store?
    /// </summary>
    public interface IBuyable
    {

        /// <summary>
        /// How much does the block cost?
        /// </summary>
        int Cost { get; set; }


        /// <summary>
        /// What's the description that should show up in the
        /// shop window?
        /// </summary>
       string ShopDescription { get; set; }
    }
}
