﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Object.Blocks.Types
{
    /// <summary>
    /// Is this block breakable into something else?
    /// </summary>
    public interface IBreakable
    {

    }
}
