﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Objects
{
    public interface IItem
    {
        [BsonIgnore()]
        string Name { get; set; }
    }
}
