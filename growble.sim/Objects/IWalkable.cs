﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Object.Blocks.Types
{
    /// <summary>
    /// Blocks that inherit this can be walked "through".
    /// Air can be walkable for example.
    /// </summary>
    public interface IWalkable
    {
    }
}
