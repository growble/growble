﻿using growble.sim.Object.Blocks.Types;
using growble.sim.Objects;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Object.Blocks
{
    public class DoorBlock : IBlock, IWalkable, IShoutOut, IDoor,IBuyable
    {
        public DoorBlock()
        {
            this.Id = 0;
            this.World = "blank";
        }
        [BsonIgnore]
        public  string Name
        {
            get
            {
                return "Door";
            }
            set
            {
            }
        }


        [BsonIgnore]
        public string ShoutOutMessage
        {
            get
            {
                return "Go to <span style='color:yellow'>" + World + "</span> door " + Id + "...";
            }
            set
            {
            }
        }



        private int id = 1;

        public string World
        {
            get
            {
                return world;
            }
            set
            {
                world = value;
            }
        }
        private string world = "";


        [BsonIgnore]
        public int Cost
        {
            get
            {
                return 100;
            }
            set
            {
            }
        }


        [BsonIgnore]
        public string ShopDescription
        {
            get
            {
                return "Buy a Door, because you're going places!";
            }
            set
            {
            }
        }

        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }
    }
}
