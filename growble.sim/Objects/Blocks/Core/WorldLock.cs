﻿using growble.sim.Object.Blocks;
using growble.sim.Object.Blocks.Types;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Objects.Blocks.Core
{
    public class WorldLock : IBlock, IItem, IBuyable, IWalkable, IBreakable, IShoutOut, ILock
    {
        

        public string ShoutOutMessage
        {
            get
            {
                return this.Owner + " owns this Lock.";
            }
            set
            {
            }
        }

        [BsonIgnore]
        public int Width
        {
            get
            {
                return 1000;
            }
            set
            {
            }
        }

        [BsonIgnore]
        public int Height
        {
            get
            {
                return 6000;
            }
            set
            {
            }
        }

        [BsonIgnore]
        public string Name
        {
            get
            {
                return "WorldLock";

            }
            set { }
        }
        
        [BsonIgnore]
        public int Cost
        {
            get
            {
                return 10000;
            }
            set
            {
            }
        }

        [BsonIgnore]
        public string ShopDescription
        {
            get
            {
                return "Will lock your entire world.";
            }
            set
            {
            }
        }


        public string Owner
        {
            get;
            set;
        }
    }
}
