﻿using growble.sim.Object.Blocks.Types;
using growble.sim.Objects;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Object.Blocks
{
    public class WorldDoorBlock : IBlock, IWalkable, IShoutOut
    {
        [BsonIgnore]
        public  string Name
        {
            get
            {
                return "WorldDoor";
            }
            set
            {
            }
        }

    [BsonIgnore]
        public string ShoutOutMessage
        {
            get
            {
                return "Leave World";
            }
            set
            {
            }
        }
    }
}
