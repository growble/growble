﻿using growble.sim.Object.Blocks.Types;
using growble.sim.Objects;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Object.Blocks
{
    public class WaterBlock : IBlock, IWalkable, IItem, IBreakable, IBuyable, IFriction
    {
        [BsonIgnore]
        public  string Name
        {
            get
            {
                return "Water";
            }
            set
            {
            }
        }

    [BsonIgnore]
        public int Cost
        {
            get
            {
                return 100;
            }
            set
            {
            }
        }

    [BsonIgnore]
        public string ShopDescription
        {
            get
            {
                return "Water is awesome...";
            }
            set
            {
            }
        }

    [BsonIgnore]
        public double Frictional
        {
            get
            {
                return -0.5;
            }
            set
            {
            }
        }
    }
}
