﻿using growble.sim.Object.Blocks;
using growble.sim.Object.Blocks.Types;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Objects.Blocks.Core
{
    public class MineLock : IBlock, IItem, IBuyable, IWalkable, IBreakable, IShoutOut, ILock
    {
        

        public string ShoutOutMessage
        {
            get
            {
                return this.Owner + " owns this Lock.";
            }
            set
            {
            }
        }

        [BsonIgnore]
        public int Width
        {
            get
            {
                return 10;
            }
            set
            {
            }
        }

        [BsonIgnore]
        public int Height
        {
            get
            {
                return 6;
            }
            set
            {
            }
        }

        [BsonIgnore]
        public string Name
        {
            get
            {
                return "MineLock";

            }
            set { }
        }
        
        [BsonIgnore]
        public int Cost
        {
            get
            {
                return 1000;
            }
            set
            {
            }
        }

        [BsonIgnore]
        public string ShopDescription
        {
            get
            {
                return "Will lock your some tiles so only you can control them! 6 High by 10 wide.";
            }
            set
            {
            }
        }


        public string Owner
        {
            get;
            set;
        }
    }
}
