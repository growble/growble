﻿using growble.sim.Object.Blocks.Types;
using growble.sim.Objects;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Object.Blocks
{
    public class WoodBlock : IBlock, IItem, IBuyable, IWalkable, IBreakable
    {
        [BsonIgnore]
        public  string Name
        {
            get
            {
                return "FloorBoards";
            }
            set
            {
            }
        }

    [BsonIgnore]
        public int Cost
        {
            get
            {
                return 300;
            }
            set
            {
            }
        }

    [BsonIgnore]
        public string ShopDescription
        {
            get
            {
                return "Awesome Floor Boards!";
            }
            set
            {
            }
        }
    }
}
