﻿using growble.sim.Object.Blocks.Types;
using growble.sim.Objects;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Object.Blocks
{
    public class GrassBlock : IBlock, IItem, IBuyable, IBreakable
    {
        [BsonIgnore]
        public  string Name
        {
            get
            {
                return "Grass";
            }
            set
            {
            }
        }

    [BsonIgnore]
        public string Decription
        {
            get
            {
                return "It's 99.9% grass. No added Sugar!";
            }
        }

        [BsonIgnore]
        public int Cost
        {
            get
            {
                return 100;
            }
            set
            {
            }
        }

        [BsonIgnore]
        public string ShopDescription
        {
            get
            {
                return "What a load of shit";
            }
            set
            {
            }
        }
    }
}
