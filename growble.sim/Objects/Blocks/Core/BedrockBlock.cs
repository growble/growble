﻿using growble.sim.Object.Blocks.Types;
using growble.sim.Objects;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Object.Blocks
{
    public class BedrockBlock : IBlock, IItem, IBuyable
    {
        [BsonIgnore]
        public string Name
        {
            get
            {
                return "BedRock";
            }
            set
            {
            }
        }

        [BsonIgnore]
        public int Cost
        {
            get
            {
                return 100;
            }
            set
            {
            }
        }

        [BsonIgnore]
        public string ShopDescription
        {
            get
            {
                return "Awesome Bedrock, totally not destroyable!";
            }
            set
            {
            }
        }
    }
}
