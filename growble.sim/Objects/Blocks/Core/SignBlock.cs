﻿using growble.sim.Object.Blocks.Types;
using growble.sim.Objects;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Object.Blocks
{
    public class SignBlock : IBlock, IItem, IBuyable, IWalkable, IBreakable, IShoutOut
    {
        public SignBlock()
        {
            this.ShoutOutMessage = "Screwdriver to change the message...";
        }

        [BsonIgnore]
        public  string Name
        {
            get
            {
                return "Sign";
            }
            set
            {
            }
        }

        [BsonIgnore]
        public int Cost
        {
            get
            {
                return 300;
            }
            set
            {
            }
        }

        [BsonIgnore]
        public string ShopDescription
        {
            get
            {
                return "Write fun signs!";
            }
            set
            {
            }
        }

        public string ShoutOutMessage
        {
            get;
            set;
        }
    }
}
