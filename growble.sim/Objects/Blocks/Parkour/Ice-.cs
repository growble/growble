﻿using growble.sim.Object.Blocks;
using growble.sim.Object.Blocks.Types;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Objects.Blocks.Parkour
{
    class Ice : IBlock, IItem, IBuyable, IWalkable,  IBreakable, IFriction
    {
        [BsonIgnore]
        public string Name
        {
            get
            {
                return "Ice";
            }
            set
            {

            }
        }

    [BsonIgnore]
        public int Cost
        {
            get
            {
                return 20;
            }
            set
            {
            }
        }

    [BsonIgnore]
        public string ShopDescription
        {
            get
            {
                return "There Death Spikes...!";
            }
            set
            {
            }
        }

    [BsonIgnore]
        public double Frictional
        {
            get
            {
                return 0.993f;
            }
            set
            {
            }
        }
    }
}
