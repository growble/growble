﻿using growble.sim.Object.Blocks;
using growble.sim.Object.Blocks.Types;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Objects.Blocks.Parkour
{

    class DeathSpikes : IBlock, IItem, IBuyable,  IBreakable,  IBouncey, IKillable
    {
        [BsonIgnore]
        public string Name
        {
            get
            {
                return "DeathSpikes";
            }
            set
            {
            }
        }

    [BsonIgnore]
        public int Cost
        {
            get
            {
                return 20;
            }
            set
            {
            }
        }

    [BsonIgnore]
        public string ShopDescription
        {
            get
            {
                return "Ice :) is :) nice.";
            }
            set
            {
            }
        }


    [BsonIgnore]
        public double Bounce
        {
            get
            {
                return 0.9999f;
            }
            set
            {
            }
        }

    [BsonIgnore]
    public bool Instadeath
    {
        get
        {
            return true;
        }
        set
        {
        }
    }
    }
}
