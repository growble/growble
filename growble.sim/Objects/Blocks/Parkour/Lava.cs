﻿using growble.sim.Object.Blocks;
using growble.sim.Object.Blocks.Types;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Objects.Blocks.Parkour
{
    class Lava : IBlock, IItem, IBuyable,  IBreakable,  IBouncey
    {
        [BsonIgnore]
        public string Name
        {
            get
            {
                return "Lava";
            }
            set
            {
            }
        }

    [BsonIgnore]
        public int Cost
        {
            get
            {
                return 20;
            }
            set
            {
            }
        }

    [BsonIgnore]
        public string ShopDescription
        {
            get
            {
                return "Ice :) is :) nice.";
            }
            set
            {
            }
        }


    [BsonIgnore]
        public double Bounce
        {
            get
            {
                return 0.9999f;
            }
            set
            {
            }
        }
    }
}
