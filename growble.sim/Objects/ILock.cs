﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Object.Blocks.Types
{
    /// <summary>
    /// Lock defines an item that locks a particular area
    /// </summary>
    public interface ILock
    {
        int Width { get; set; }
        int Height { get; set; }
        string Owner { get; set; }
    }
}
