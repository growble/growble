﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Objects
{
    /// <summary>
    /// IShoutout means that the object 'shouts out' when the player walks over it
    /// </summary>
    public interface IShoutOut
    {
        string ShoutOutMessage { get; set; }
    }
}
