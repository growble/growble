﻿using growble.sim.Objects;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim.Object.Blocks
{
    /// <summary>
    /// Touching the block kills the player!
    /// </summary>
    public interface IKillable
    {
        /// <summary>
        /// Instantly kills the player?
        /// </summary>
        bool Instadeath { get; set; }
    }
}
