﻿using growble.sim.Object.Blocks;
using growble.sim.Objects;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.sim
{
    public class IRepo
    {
        protected MongoDatabase Database { get; set; }

        static IRepo()
        {
            foreach (var type in typeof(IItem).Assembly.GetTypes())
            {
                if (type.IsInterface == false)
                    BsonClassMap.LookupClassMap(type);
            }
        }

        public IRepo()
        {
            var connectionString = "mongodb://localhost";
            var client = new MongoClient(connectionString);
            var server = client.GetServer();
            this.Database = server.GetDatabase("growble");

        }
    }
}
