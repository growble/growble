﻿using growble.sim.Object;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Results;

namespace growble.server.start
{
    [EnableCors("*","*","*")] 
    public class ValuesController : ApiController
    {
        // GET api/values 
        public JsonResult<WorldObject> Get()
        {

            lock (ServerProcess.WorldDataStatic)
            {
                return Json<WorldObject>(ServerProcess.WorldDataStatic.World);
            }

        }

        // GET api/values/5 
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values 
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5 
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5 
        public void Delete(int id)
        {
        }
    } 
}
