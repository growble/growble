﻿using growble.Hubs;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace growble.server.start
{
    [assembly: OwinStartup(typeof(growble.server.start.Startup))]
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //var x = new HubConfiguration();
            GlobalHost.DependencyResolver.Register(
            typeof(ServerHub),
            () => new ServerHub(ServerProcess.WorldDataStatic));


            GlobalHost.Configuration.DefaultMessageBufferSize = 1000;

            HttpConfiguration config = new HttpConfiguration();
           
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            config.EnableCors(); //WE need cross domain support.

            app.UseWebApi(config);

            app.Map("/signalr", map =>
            {
                // Setup the CORS middleware to run before SignalR.
                // By default this will allow all origins. You can 
                // configure the set of origins and/or http verbs by
                // providing a cors options with a different policy.

                map.UseCors(CorsOptions.AllowAll);
                var hubConfiguration = new HubConfiguration
                {
                    // You can enable JSONP by uncommenting line below.
                    // JSONP requests are insecure but some older browsers (and some
                    // versions of IE) require JSONP to work cross domain
                    // EnableJSONP = true

                };
                // Run the SignalR pipeline. We're not using MapSignalR
                // since this branch already runs under the "/signalr"
                // path.



                map.RunSignalR(hubConfiguration);
            });


        }
    }


}
