﻿using growble.Hubs;
using growble.server.start;
using growble.sim.Data.Repo;
using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace growble.server
{
    public class ServerStartup
    {
        public static ServerProcess servProc;

        public static void Main(string[] args)
        {
            string worldname = args[0];


            var ass = AppDomain.CurrentDomain.Load(typeof(ServerHub).Assembly.FullName);
            AppDomain.CurrentDomain.Load(typeof(Startup).Assembly.FullName);
            
            //Create the server
            servProc = new ServerProcess(worldname);
            
            //Initialise
            servProc.Init();

            //Set Shutdown Code

            string coreUrl = "http://www.growble.net/:";

            Console.Out.WriteLine(System.Environment.MachineName);
            if (System.Environment.MachineName == "ANTHONY")
            {
                coreUrl = "http://localhost:";
            }

            string url = coreUrl + ServerStartup.GetOpenPort();


            //Make Sure we shutdown properly
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(CurrentDomain_ProcessExit);
            

            using (WebApp.Start<Startup>(url))
            {

                Console.WriteLine("World: {0}", worldname);
                Console.WriteLine("Server running on {0}", url);

                //Let the process know we've started.
                servProc.Started(worldname, url);
                

                Thread servProcThread = new Thread(new ThreadStart(servProc.Run));
                servProcThread.Start();
                
                while(servProcThread.IsAlive)
                {
                    Thread.Sleep(1000);
                }

                //Cleanup
                servProc.OnExit();

                
                


            }
        }

        static void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
        }

        private static string GetOpenPort()
        {
            int PortStartIndex = 22200;
            int PortEndIndex = 40000;

            Random r = new Random((int)DateTime.Now.Ticks);

            return (PortStartIndex + r.Next(10000)).ToString();
        }
        
    }
}
