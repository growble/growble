{"frames": [

{
	"filename": "Air.png",
	"frame": {"x":2,"y":2,"w":32,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":32,"h":32},
	"sourceSize": {"w":32,"h":32},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "BedRock.png",
	"frame": {"x":36,"y":2,"w":32,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":32,"h":32},
	"sourceSize": {"w":32,"h":32},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "BrainBody.png",
	"frame": {"x":70,"y":2,"w":24,"h":19},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":11,"w":24,"h":19},
	"sourceSize": {"w":32,"h":32},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "BrainHelmet.png",
	"frame": {"x":96,"y":2,"w":24,"h":13},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":0,"w":24,"h":13},
	"sourceSize": {"w":32,"h":32},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "Brick.png",
	"frame": {"x":122,"y":2,"w":32,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":32,"h":32},
	"sourceSize": {"w":32,"h":32},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "DaVinci.png",
	"frame": {"x":156,"y":2,"w":32,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":32,"h":32},
	"sourceSize": {"w":32,"h":32},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "DeathSpikes.png",
	"frame": {"x":190,"y":2,"w":32,"h":31},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":32,"h":31},
	"sourceSize": {"w":32,"h":31},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "Door.png",
	"frame": {"x":224,"y":2,"w":32,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":32,"h":32},
	"sourceSize": {"w":32,"h":32},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "EvilPenguin.png",
	"frame": {"x":258,"y":2,"w":23,"h":29},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":3,"w":23,"h":29},
	"sourceSize": {"w":32,"h":32},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "FloorBoards.png",
	"frame": {"x":283,"y":2,"w":32,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":32,"h":32},
	"sourceSize": {"w":32,"h":32},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "Grass.png",
	"frame": {"x":317,"y":2,"w":32,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":32,"h":32},
	"sourceSize": {"w":32,"h":32},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "Hammer.png",
	"frame": {"x":351,"y":2,"w":32,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":32,"h":32},
	"sourceSize": {"w":32,"h":32},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "Ice.png",
	"frame": {"x":2,"y":36,"w":32,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":32,"h":32},
	"sourceSize": {"w":32,"h":32},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "Lava.png",
	"frame": {"x":36,"y":36,"w":32,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":32,"h":32},
	"sourceSize": {"w":32,"h":32},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "MineLock.png",
	"frame": {"x":70,"y":36,"w":28,"h":32},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":0,"w":28,"h":32},
	"sourceSize": {"w":32,"h":32},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "Sand.png",
	"frame": {"x":100,"y":36,"w":32,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":32,"h":32},
	"sourceSize": {"w":32,"h":32},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "ScrewDriver.png",
	"frame": {"x":134,"y":36,"w":32,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":32,"h":32},
	"sourceSize": {"w":32,"h":32},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "Sign.png",
	"frame": {"x":168,"y":36,"w":32,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":32,"h":32},
	"sourceSize": {"w":32,"h":32},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "Suit.png",
	"frame": {"x":202,"y":36,"w":22,"h":14},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":5,"y":17,"w":22,"h":14},
	"sourceSize": {"w":32,"h":32},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "Water.png",
	"frame": {"x":226,"y":36,"w":126,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":126,"h":32},
	"sourceSize": {"w":126,"h":32},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "WorldDoor.png",
	"frame": {"x":354,"y":36,"w":32,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":32,"h":32},
	"sourceSize": {"w":32,"h":32},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "WorldLock.png",
	"frame": {"x":2,"y":70,"w":28,"h":32},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":0,"w":28,"h":32},
	"sourceSize": {"w":32,"h":32},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "gem.png",
	"frame": {"x":32,"y":70,"w":64,"h":61},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":61},
	"sourceSize": {"w":64,"h":64},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "heart.png",
	"frame": {"x":98,"y":70,"w":170,"h":150},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":170,"h":150},
	"sourceSize": {"w":170,"h":150},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "picker.png",
	"frame": {"x":270,"y":70,"w":32,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":32,"h":32},
	"sourceSize": {"w":32,"h":32},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "player.png",
	"frame": {"x":304,"y":70,"w":25,"h":28},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":1,"w":25,"h":28},
	"sourceSize": {"w":32,"h":32},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "playerArm.png",
	"frame": {"x":331,"y":70,"w":6,"h":10},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":5,"w":6,"h":10},
	"sourceSize": {"w":8,"h":16},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "playerFoot.png",
	"frame": {"x":339,"y":70,"w":4,"h":8},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":0,"w":4,"h":8},
	"sourceSize": {"w":8,"h":8},
	"pivot": {"x":0.5,"y":0.5}
},
{
	"filename": "puff.png",
	"frame": {"x":345,"y":70,"w":32,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":32,"h":32},
	"sourceSize": {"w":32,"h":32},
	"pivot": {"x":0.5,"y":0.5}
}],
"meta": {
	"app": "http://www.codeandweb.com/texturepacker",
	"version": "1.0",
	"image": "Content.png",
	"format": "RGBA8888",
	"size": {"w":388,"h":251},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:fd3f50bdcb1f38c89e6c67ff11308fd3:386e3fc96701c8e859a4108bb6ec833e:c83e45ebbc55e9c8e91b71d1581eae9d$"
}
}
