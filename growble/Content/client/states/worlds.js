﻿var worldconn = null;
var haveTheWorld = false;

var PlayersList = {};


var worldChosen = {

    create: function () {
        var loadingSrite = $('<div class="loading pulsor">Loading World...</div>').appendTo($('#game-canvas-gui-forms'));

        console.log("error!");

    }
}

var worldsState = {

    create: function () {

        $('#game-canvas-gui-forms').html('');

        var worldSelect = $('<div class="world-input"><span>Go To World: </span></div>').appendTo($('#game-canvas-gui-forms'));
        var worldinput = $('<input/>').appendTo(worldSelect);
        var go = $('<submit>Go!</submit>').appendTo(worldSelect);

        core.invoke("getWorlds");

        if (serverworld != "")
        {
            core.invoke("joinWorld", serverworld);
        }


        ///Display to user
        core.on("startWorlds", function (worlds) {

            var fontsize = 60;

            for (x in worlds)
            {
                var button = $('<div class="start-world" style="-webkit-animation-delay: ' + 1000*Math.random() + 'ms;font-size:' + fontsize + 'px">' + worlds[x] + '</div>').appendTo($('#game-canvas-gui-forms'));
                 button.click(function () {

                     core.invoke("joinWorld", this.valueOf());

                     

                     game.state.add('worldChosen', worldChosen);
                     game.state.start('worldChosen');

                     $("*").unbind("click");

                 }.bind(worlds[x]));

                 fontsize = fontsize - 1;
            }
        });

        go.click(function () {

            console.log(worldinput.val());
            core.invoke("joinWorld", worldinput.val());


            game.state.add('worldChosen', worldChosen);
            game.state.start('worldChosen');


            $("*").unbind("click");

        });

    },

}

core.on("connectToWorld", function (connString) {

    var worldConnection = $.hubConnection(connString);


    worldHub = worldConnection.createHubProxy('worldHub');


    //Start
    ActiveWorld = new GameWorld();
    
    worldHub.on("setWorld", function (sworld, splayers, splayer) {

        $('.loading').remove();
        $('#game-canvas-gui').css('background-image', 'none');
        


        $.ajax({
            url: connString + '/api/values',
            dataType: 'JSON',
            type: 'GET',
            success: function (data) {


                console.log("set world");

                game.state.add('worldPlay', ActiveWorld);
                game.state.start('worldPlay');

                worldHub.invoke("loadMe");

                ActiveWorld.InitWorld(data, splayers, splayer);

            }
        });


    });

    worldHub.on("playerAdded", function (player) {

        //PlayersList[player.Name] = player;
        ActiveWorld.addPlayer(player);
    });

    worldHub.on("playerRemoved", function (player) {

        //PlayersList[player.Name] = player;
        ActiveWorld.removePlayer(player);
    });

    worldHub.on("updatePlayerPositions", function (players) {

        ActiveWorld.playerPositionsUpdate(players);
    });

    worldHub.on("SetGems", function (gems) {

        $('#gemcount').html(gems);

        $('#gemcount').effect("shake");
    });


    worldHub.on("blockSet", function (x, y, name) {

        ActiveWorld.blockSet(x, y, name);

    });

    worldHub.on("blockDestroyed", function (x,y) {

        ActiveWorld.blockDestroyed(x, y);
    });
    
    worldHub.on("leaveWorld", function () {

        window.location.href = "http://" + location.host + "/client/";
    });

    worldHub.on("goToWorld", function (world) {

        window.location.href = "http://" + location.host + "/client/" + world;
    });

    worldHub.on("setMassBlocks", function (blocks) {
        for (var i in blocks)
        {
            ActiveWorld.blockSet(blocks[i].x, blocks[i].y, "Grass");
        }

    });

    worldHub.connection.start().done(function () {


        console.log("Connected to World");
        

        worldHub.invoke("Authenticate", servername);


    }).done(function () {

        console.log("Connected to Server Core");

    }).fail(function (error) {
        console.log(error);
    });


});

