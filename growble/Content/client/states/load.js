﻿var start = "../";
var loadState = {

    preload: function()
    {
        game.load.atlas('game', start + 'Content/assets/Content.png', start + '/Content/assets/content.js');
        game.load.image('background', start + 'Content/background.jpg');

    },
    create: function()
    {

        $('<div class="progress-bar orange stripes"><span class="progress-bar-span" style="width: 1%"></span></div>').appendTo($('#game-canvas-gui'));


    },
    loadUpdate: function (progress) {

        $('.progress-bar-span').width(game.load.progress + '%');
        console.log(game.load.progress);

        if (game.load.progress > 99)
        {
            $('.progress-bar').remove();

            game.state.add('login', loginState);
            game.state.start('login');
        }
    }
}
