﻿var game = new Phaser.Game($(window).width(), $(window).height(), Phaser.AUTO, "game-canvas");


var mainConnection = $.hubConnection("../../signalr/hubs");
var worldConnection = null;


var core = mainConnection.createHubProxy('coreHub');
core.logging = true;
var worldHub = null;
var ActiveWorld = null;
var ZoomScale = 1;
var connectedToCore = false;

core.connection.start().done(function () {

    connectedToCore = true;

    game.state.add('load', loadState);
    game.state.start('load');

}).fail(function (error) {
    console.log(error);
});

$(window).resize(function () { window.resizeGame(); })

function resizeGame() {
    var height = $(window).height();
    var width = $(window).width();

    game.width = width;
    game.height = height;
    game.stage.bounds.width = width;
    game.stage.bounds.height = height;

    if (game.renderType === Phaser.WEBGL) {
        game.renderer.resize(width, height);
    }

}