﻿

function Tile( x, y, type, emitter)
{
    this.X = x;
    this.Y = y;
    this.BlockType = type;
    this.AO = 1;
    //this.ParticleEmmiter = game.add.emitter(0, 0, 100);
    //this.ParticleEmmiter.z = 120;
    //this.ParticleEmmiter.width =128;
    //this.ParticleEmmiter.height = 128;

    ///Create the sprite
    this.Sprite = new Phaser.Sprite(game, x * global.scale, y * global.scale, "game",type + ".png");
    this.Sprite.scale.x = global.mscale;
    this.Sprite.scale.y = global.mscale;


    this.Sprite.inputEnabled = true;
    this.Sprite.events.onInputDown.add(this.onClick.bind(this), game);
    this.Sprite.input.priorityID = 100;

    this.Sprite.input.pixelPerfectClick = false;
    //this.Sprite.autoCull = true;

    this.Sprite.visible = true;

}

//What the tile should do when clicked
Tile.prototype.onClick = function (pointer)
{
    ActiveWorld.Player.crayhands(this.X, this.Y);

    worldHub.invoke("Click", this.X, this.Y);

}

///Calculates the ambient occlusion of this tile
Tile.prototype.calcAO = function(Tiles)
{
    var pass = 3;
    var airblocks = 0;
    for (i = this.X - pass+1; i < this.X + pass ; i++)
        for (j = this.Y - pass +1; j < this.Y + pass ; j++)
        {
            if (Tiles[i])
                if (Tiles[i][j])
                    if (Tiles[i][j].BlockType != "Grass")
                    {
                        airblocks++;
                    }
        }

    this.AO = airblocks / (pass * pass * 4);
    if (this.AO > 1)
        console.log("error - massive fuckup");
    this.AO = Math.ceil(this.AO * 145 + 105);
    if (this.AO > 255)
        this.AO = 245;


    this.Sprite.tint = Phaser.Color.RGBtoString(this.AO, this.AO, this.AO, 0, "0x");

   
}
