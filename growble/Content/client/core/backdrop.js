﻿function backdrop(name, player)
{
    this.Player = player;
    this.Background = game.add.tileSprite(0, 0, 30000, 30000, "background");
    this.Background.fixedToCamera = true;
    this.Background.z = -1;
    this.Background.scale.x = 1.4;
    this.Background.scale.y = 1.4;
}


backdrop.prototype.update = function () {

    if (this.Player != null)
    {
        this.Background.cameraOffset.x = -200 - 10 * this.Player.X;
        this.Background.cameraOffset.y = -200 - 10 * this.Player.Y;
        
    }

    

};