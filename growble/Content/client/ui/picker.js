﻿
var pickerSprite = null;

function initPicker()
{

    var x = Math.floor(game.input.worldX / global.scale);
    var y = Math.floor(game.input.worldY / global.scale);


    pickerSprite = new Phaser.Sprite(game,0,0, "SpritePicker");
    pickerSprite.scale.x = global.mscale;
    pickerSprite.scale.y = global.mscale;

    GuiLayer.add(pickerSprite);

}

function UpdatePicker()
{

    var x = Math.floor(game.input.mousePointer.worldX / global.scale);
    var y = Math.floor(game.input.mousePointer.worldY / global.scale);

    pickerSprite.position.x = x * global.scale;
    pickerSprite.position.y = y * global.scale;

}