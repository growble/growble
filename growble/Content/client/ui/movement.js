﻿
var left = null;
var right = null;
var jump = null;
var up = null;
var down = null;

function initKeyboard()
{
    $('#game-canvas-gui').addClass("canvas-unclickable");
    jump = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    jump.onDown.add(playerJump, this);

    left = game.input.keyboard.addKey(Phaser.Keyboard.A);
    left.onDown.add(playerLeft, this);
    left.onUp.add(playerStop, this);

    right = game.input.keyboard.addKey(Phaser.Keyboard.D);
    right.onDown.add(playerRight, this);
    right.onUp.add(playerStop, this);


    up = game.input.keyboard.addKey(Phaser.Keyboard.S);
    up.onDown.add(playerUp, this);
    up.onUp.add(playerStopY, this);


    down = game.input.keyboard.addKey(Phaser.Keyboard.W);
    down.onDown.add(playerDown, this);
    down.onUp.add(playerStopY, this);

   


}

function disableKeyboard()
{

    $('#game-canvas-gui').removeClass("canvas-unclickable");
    game.input.keyboard.reset();
    game.input.keyboard.removeKey(Phaser.Keyboard.A);
    game.input.keyboard.removeKey(Phaser.Keyboard.D);
    game.input.keyboard.removeKey(Phaser.Keyboard.SPACEBAR);
    game.input.keyboard.removeKey(Phaser.Keyboard.W);
    game.input.keyboard.removeKey(Phaser.Keyboard.S);
}

function playerLeft()
{
    console.log("!");

    worldHub.invoke("RequestMove", -1);
}

function playerRight()
{

    worldHub.invoke("RequestMove", 1);
}


function playerUp() {

    worldHub.invoke("RequestMoveUp", 1);
}


function playerDown() {

    worldHub.invoke("RequestMoveUp", -1);
}

function playerStop() {

    worldHub.invoke("RequestMove", 0);
}

function playerStopY() {

    worldHub.invoke("RequestMoveUp", 0);
}


function playerJump()
{

    worldHub.invoke("RequestJump");
}
