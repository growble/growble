﻿ 
function Shop()
{
    this.RewardsDiv = $('<div class="rewards"></div>').appendTo($('#game-canvas-gui'));
    this.ShopDiv = $('<div class="shop"></div>').appendTo($('#game-canvas-gui'));
    this.ShopDivHeader = $('<div class="shopheader"></div>').appendTo(this.ShopDiv);

    $('<h1>Gem Store!</h1>').appendTo(this.ShopDivHeader);
    this.BuyGemsButton = $('<a>Wach video to get 100 Gems!</a>').appendTo(this.ShopDivHeader);
    this.CloseShop = $('<a>Close</a>').appendTo(this.ShopDivHeader);



    this.BuyGemsButton.click(function () {
        this.buyGems();
    }.bind(this));

    this.ShopDiv.hide();

    this.CloseShop.click(function () {

        this.ShopDiv.hide();
    }.bind(this));

    worldHub.on("initCatalog", function (cat) {

        this.initCatalog(cat);
    }.bind(this));

    worldHub.on("seeReward", function (rew) {
        /*
             Need to let user claim the reward...
        */
        $('.rewards').html('');
        for (var x in rew)
        {

            var rewards = $('<div class="reward">Claim ' + rew[x].Gems + ' Gems!</div>').appendTo($('.rewards'));
            rewards.click(function () {
                console.log(this.id);
                worldHub.invoke("claimReward", this.id); //Fetch some rewards...

                this.reward.remove();
                $('.rewards').html('')

            }.bind({ reward : rewards, id : rew[x].Id }));
        }
    });
}

Shop.prototype.buyGems = function()
{

    ViroolWidgetModal.open('https://api.virool.com/widgets/d97bc34?width=640&height=400&gender=a', {width: 640, height: 400});

}


Shop.prototype.show = function () {

    worldHub.invoke("rewards"); //Fetch some rewards...
    this.ShopDiv.show(2000);

}

Shop.prototype.initCatalog = function (cat) {


    this.catalogDiv = $('<div class="itemcatalog"></div>').appendTo(this.ShopDiv);
    this.itemsscrollholder = $('<div class="itemscrollholder"></div>').appendTo(this.ShopDiv);

    var next = $('<div class="next">Back</div>').appendTo(this.itemsscrollholder);
    this.itemsScroll = $('<div class="itemscroll"></div>').appendTo(this.itemsscrollholder);
    var back = $('<div class="prev">More</div>').appendTo(this.itemsscrollholder);

    this.itemDetails = $('<div class="itemsdetails"></div>').appendTo(this.catalogDiv);
    next.click(function () {

        var y = $('.itemscroll').scrollTop();  //your current y position on the page
        $('.itemscroll').scrollTop(y - 100);

    });

    back.click(function () {

        var y = $('.itemscroll').scrollTop();  //your current y position on the page
        $('.itemscroll').scrollTop(y + 100);

    });

    for (var i in cat)
    {

        var shopitem = $('<div class="shop-item"></div>').appendTo(this.itemsScroll)
        //$('<p>' + cat[i].Name + ' </p>').appendTo(shopitem);

        var inv = "<img src='/Content/assets/sprites/" + cat[i].Name + ".png' />";


        var button = $(inv).clone().appendTo(shopitem);

        button.click(function () {

            $('.itemsdetails').html(' ');

            $('<label>Name: </label><p>' + this.Name + '</p><br/>').appendTo($('.itemsdetails'));
            $('<label>Description: </label><p>' + this.ShopDescription + '</p><br/>').appendTo($('.itemsdetails'));
            $('<label>Cost: </label><p>' + this.Cost + ' Gems</p> <br/>').appendTo($('.itemsdetails'));

            var buyButton = $('<a class="buybutton"> Buy 1 </a>').appendTo($('.itemsdetails'));
            var buyButton1 = $('<a class="buybutton"> Buy 2 </a>').appendTo($('.itemsdetails'));
            var buyButton2 = $('<a class="buybutton"> Buy 5 </a>').appendTo($('.itemsdetails'));
            var buyButton3 = $('<a class="buybutton"> Buy 10 </a>').appendTo($('.itemsdetails'));


            //Buy Button
            buyButton.click(function () {
                worldHub.invoke("BuyItem",this, 1);
            }.bind(this.Name));

            buyButton1.click(function () {
                worldHub.invoke("BuyItem", this, 2);
            }.bind(this.Name));

            buyButton2.click(function () {
                worldHub.invoke("BuyItem", this, 5);
            }.bind(this.Name));

            buyButton3.click(function () {
                worldHub.invoke("BuyItem", this, 10);
            }.bind(this.Name));

        }.bind(cat[i]));

    }



}
