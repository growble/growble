﻿using System.Web;
using System.Web.Optimization;

namespace growble
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));

            /*
             * 
             <script src="~/Scripts/jquery-1.8.2.min.js"></script>

            <script src="~/Scripts/jquery.signalR-2.1.2.js"></script>


            <script src="~/Scripts/phaser.js"></script>
            <script src="~/Content/client/main.js"></script>
            
             */
            bundles.Add(new ScriptBundle("~/client/game").Include(
                "~/Content/client/main.js",
                "~/Content/client/ui/shop.js",
            "~/Content/client/ui/hud.js",
            "~/Content/client/core/player.js",
            "~/Content/client/core/tile.js",
            "~/Content/client/core/backdrop.js",
            "~/Content/client/core/gameworld.js",
            "~/Content/client/ui/movement.js",
            "~/Content/client/ui/hud.js",
            "~/Content/client/ui/picker.js",
            "~/Content/client/states/load.js",
            "~/Content/client/states/worlds.js",

            "~/Content/client/states/worldPlay.js",
            "~/Content/client/states/register.js",
            "~/Content/client/states/login.js",
            "~/Content/client/start.js"));


            BundleTable.EnableOptimizations = true;


        }
    }
}