﻿using growble.Models;
using growble.sim.Data.Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace growble.Controllers
{
    public class GameController : Controller
    {
        // GET: Game
        public ActionResult Shop()
        {
            GemShopModel gsm = new GemShopModel();

            gsm.catalog = new sim.Data.Catalog();
            return View(gsm);
        }

        public byte VideoReward(string user_id, int reward)
        {
            RewardRepo rr = new RewardRepo();
            rr.AddReward(new growble.sim.Data.Reward() {  DateTime = DateTime.Now, Gems = reward, User = user_id});
            
            return 0;
        }
    }
}