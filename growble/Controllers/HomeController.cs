﻿using growble.sim.Data.Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace growble.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            return View();
        }


        public ActionResult Client(string name)
        {
            if (String.IsNullOrEmpty(name))
            {
                name = "";
            }
            if (this.User.Identity.IsAuthenticated)
            {
                //Name
                ViewBag.World = name;
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public ActionResult Community()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult News()
        {
            return View();
        }

        public ActionResult Blog()
        {
            return View();
        }

        public ActionResult Article(string id)
        {
            WebRepo wrep = new WebRepo();
            return View(wrep.GetArticle(id));
        }
    }
}
