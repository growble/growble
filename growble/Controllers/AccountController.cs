﻿using growble.Models;
using growble.sim.Data.Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace growble.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult Me()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.error = "Wrong username or password bob.";
                return View();
            }
            else
            {
                AccountRepo arep = new AccountRepo();
                if(arep.IsValidLogin(model.Username, model.Password) == null)
                {

                    ViewBag.error = "Wrong username or password bob.";
                    return View();
                }
                else
                {
                    FormsAuthentication.SetAuthCookie(model.Username, true);
                    return RedirectToAction("Index", "Home");
                }

                return View();
            }
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {

            if (!ModelState.IsValid)
            {
                ViewBag.error = "You need to put proper data... username and password have to be between 3 and 20 chars... data for all!";
                return View();
            }

            AccountRepo arep = new AccountRepo();

            model.Username = AccountController.GetNonNumericData(model.Username).ToLower();

            if(arep.IsUsernameAvailable(model.Username))
            {
                if (ModelState.IsValid)
                {
                    //Create the new account
                    arep.NewAccount(model.Username, model.Email, model.Password);
                    return RedirectToAction("Login", "Account");
                }
                else
                {

                    ViewBag.error = "You need to put proper data... username and password have to be between 3 and 20 chars... data for all!";
                    return View();
                }
                
                return View();
            }
            else
            {
                ViewBag.error = "That username is allready in use...";
                return View();
            }
        }

        [HttpGet]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Buy()
        {
            return View();
        }
        /// <summary>
        /// id - ID of this transaction.
        /*
            uid - ID of the user which performed this transaction. 
            oid - ID of the Offer.
            new - Number of in-game currency your user has earned by completing this Offer.
            total - Total number of in-game currency your user has earned on this App.
            sig - Security Hash used to verify the authenticity of the Postbacks we send to your web server. Use your App's Secret Key to calculate a matching Security Hash (MD5) as follows and then compare it to sig. These strings should match, otherwise you should not allow the Postback.
            To get the Secret Key for your App, login at https://pub.superrewards.com, go to the Apps page, select Edit, then Integrate. The Secret Key is on this page.
            sig = md5(id:new:uid:SECRET_KEY)
            In PHP, this would be: 
    */    
    /// </summary>
        /// <returns></returns>
        public byte Confirm(string uid, string oid, string New,string total, string sig)
        {
            return (byte)1;
        }
        public static string GetNonNumericData(string input)

        {

            Regex regex = new Regex("[^a-zA-Z0-9]");

            return regex.Replace(input, "");

        }

    }
}
