﻿using growble.Hubs;
using growble.server.Core;
using growble.server.Grow;
using growble.server.Physics;
using growble.server.Regen;
using growble.sim.Data;
using growble.sim.Data.Repo;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace growble.server
{
    public class ServerProcess
    {
        public bool Running { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }

        public WorldData WorldData { get; set; }
        public World World { get; set; }
        public static WorldData WorldDataStatic { get; set; }

        public List<IManager> Managers { get; set; }
        public DateTime LastPing { get; set; }

        public PhysicsManager Physics { get; set; }
        public GemManager GemManager { get; set; }
        public LifeManager LifeManager { get; set; }
        public RegenManager RegenManager { get; set; }

        

        public ServerProcess(string worldName)
        {
            this.Name = worldName;
            this.LastPing = DateTime.Now;
        }

        /// <summary>
        /// When the Process first starts we need to get the 
        /// World Data initialised.
        /// 
        /// Need to make that data staticly accessible
        /// </summary>
        public void Init()
        {
            //Initialise World Data
            this.WorldData = new server.WorldData();


            WorldRepo wrep = new WorldRepo();
            if (wrep.WorldExists(this.Name))
            {
                this.World = wrep.GetWorld(this.Name);
                this.WorldData.World = this.World.WorldObject;
            }
            else
            {
                wrep.CreateWorld(this.Name);
                this.World = wrep.GetWorld(this.Name);
                this.WorldData.World = this.World.WorldObject;
            }

            //Make Static
            ServerProcess.WorldDataStatic = this.WorldData;


            //Create Hub Context
            this.WorldData.Hub = GlobalHost.ConnectionManager.GetHubContext<ServerHub>();

            //Iniitalise Managers
            Managers = new List<IManager>();
            Managers.Add(this.Physics = new PhysicsManager(this.WorldData));
            Managers.Add(this.RegenManager = new RegenManager(this.WorldData));
            Managers.Add(this.GemManager = new GemManager(this.WorldData));
            Managers.Add(this.LifeManager = new LifeManager(this.WorldData));
        }

        public void Started(string name, string url)
        {
            //Set Data 
            this.Name = name;
            this.Url = url;

            WorldRepo wrep = new WorldRepo();
            wrep.StartWorld(new sim.Data.OnlineWorld() { Name = name, Connection = url });

            this.Running = true;
        }

        public void Run()
        {
            try
            {
                while(this.Running)
                {
                    this.Update();
                    Thread.Sleep(16);

                    if (this.WorldData.Players.Count > 0)
                        this.LastPing = DateTime.Now;
                    else
                    {
                        if ((DateTime.Now - this.LastPing).TotalMilliseconds > 14 * 1000)
                        {
                            //We need to close down
                            this.Running = false;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Console.Out.WriteLine(ex.ToString());
            }
            finally
            {
                this.OnExit();
            }
           

        }

        public void Update()
        {
            ///Update those managers
            foreach (var manager in this.Managers)
            {
                //Lock the world
                lock(this.WorldData)
                {
                    manager.Update();
                }
            }
        }

        /// <summary>
        /// Called when the Process has to close fairly quickly.
        /// </summary>
        public void OnExit()
        {

            //End the world when we exit
            WorldRepo wrep = new WorldRepo();
            wrep.UpdateWorld(this.World); // Update the world

            wrep.EndWorld(new sim.Data.OnlineWorld() { Name = this.Name, Connection = this.Url });
            
            Console.Out.WriteLine("Server Process Thread Dead");
        }
    }
}
