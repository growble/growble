﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using growble.sim.Object;
using growble.sim.Data;
using System.Collections.Concurrent;
using Microsoft.AspNet.SignalR;

namespace growble.server
{
    /// <summary>
    /// World Data is a shared data object, 
    /// holds all the worlds persistant infomation.
    /// </summary>
    public class WorldData
    {
        public WorldObject World { get; set; }
        public List<Player> Players { get; set; }
        //public ConcurrentBag<Player> Players { get; set; }
        public IHubContext Hub { get; set; }

        public WorldData()
        {
            this.World = new WorldObject();
            this.Players = new List<Player>();
        }

    }
}
