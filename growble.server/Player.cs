﻿using growble.sim.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.server
{
    public class Player
    {
        [JsonIgnore]
        public Account Account { get; set; }

        [JsonIgnore]
        public string ConnectionId { get; set; }

        public string Name { get; set; }

        public double X { get; set; }
        public double Y { get; set; }

        public double VelX { get; set; }
        public double VelY { get; set; }

        public double ImpX { get; set; }
        public double ImpY { get; set; }

        public byte Life { get; set; }

        [JsonIgnore]
        public DateTime LastGems { get; set; }
    }
}
