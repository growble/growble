﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.server
{
    /// <summary>
    /// Base Manager For handling server updates etc..
    /// </summary>
    public class IManager
    {

        protected WorldData WorldData { get; set; }

        public IManager(WorldData data)
        {
            this.WorldData = data;
        }

        public virtual void Update()
        {

        }
    }
}
