﻿using growble.sim.Object.Blocks;
using growble.sim.Objects;
using growble.sim.Objects.Items.Tools;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.Hubs
{
    public partial class ServerHub : Hub
    {
        public void Input(int x, int y)
        {


            //Get the tile
            var tile = this.WorldData.World.GetTile(x, y);

            //Clicking the door - we should leave world!
            if (tile.Block is WorldDoorBlock)
            {
                this.LeaveWorld();
            }
            else if (tile.Block is DoorBlock)
            {
                this.PortalClick(tile);

            }

        }

        private void PortalClick(Tile t)
        {
            string worldString = (t.Block as DoorBlock).World;


            Console.Out.WriteLine(t.X + " " + t.Y + " " +worldString);
            if (worldString.Contains(':'))
            {
                try
                {

                    string[] args = worldString.Split(':');

                    int x = int.Parse(args[0]);
                    int y = int.Parse(args[1]);

                    this.GetPlayer().X = (double)x;
                    this.GetPlayer().Y = (double)y;
                }
                catch(Exception ex)
                {

                    this.Clients.Caller.errorMessage("This door doesn't have a valid destination.");
                }
                
            }
            else
            {
                this.Clients.Caller.goToWorld(worldString);
            }
        }

    }
}
