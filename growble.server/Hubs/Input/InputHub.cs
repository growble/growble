﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin;
using growble.server;
using growble.sim.Data.Repo;
using growble.sim.Object.Blocks;
using growble.sim.Objects.Items.Tools;
using growble.sim.Object.Blocks.Types;
using growble.sim.Objects;
using System.Drawing;

namespace growble.Hubs
{
    public partial class ServerHub : Hub
    {
        /// <summary>
        /// The PLayer is attempting to break block [X,Y]
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void Click(int x, int y)
        {
            var player = this.GetPlayer();
            double d_2 = ((double)x - player.X) * ((double)x - player.X) + ((double)y - player.Y) * ((double)y - player.Y);

            ///If the Player is more than 5 blocks away, don't do anything
            if (d_2 > 25)
            {
                return;
            }

            //Or search for the correct handler based on the tool.
            var tool = this.GetPlayer().Account.PlayerInventory.Hand;
            if (tool == null)
            {
                this.Input(x, y);
            }
            else if (tool is Hammer)
            {
                if (IsValid(x,y))
                    this.HammerInput(x, y);
            }
            else if (tool is Screwdriver)
            {
                if (IsValid(x, y))
                    this.ScrewDriverInput(x, y);
            }
            else if (tool is IBlock)
            {
                if (IsValid(x, y))
                    this.IBlockInput(x, y);
            }



        }

        /// <summary>
        /// Is this a valid thing to do at (x,y).
        /// 
        /// Or is a Lock in operation over that point?
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bool IsValid(int x, int y)
        {
            try
            {
                foreach (var lck in this.WorldData.World.Locks)
                {
                    ILock theLock = lck.Block as ILock;

                    Rectangle rect = new Rectangle(lck.X - theLock.Width / 2, lck.Y - theLock.Height / 2, theLock.Width, theLock.Height);
                    if (rect.Contains(x,y) && theLock.Owner != this.GetPlayer().Name)
                    {
                        this.Clients.Caller.errorMessage(theLock.Owner + " has locked that...");
                        return false;
                    }
                }
            }
            catch(Exception ex)
            {
                Console.Out.WriteLine(ex.ToString());
            }

            return true;
        }

        public bool IsLockValid(int x, int y, ILock potentialLock)
        {
            foreach (var lck in this.WorldData.World.Locks)
            {
                ILock theLock = lck.Block as ILock;
                Rectangle rect = new Rectangle(lck.X - theLock.Width / 2, lck.Y - theLock.Height / 2, theLock.Width, theLock.Height);

                Rectangle potentialRect = new Rectangle(x - potentialLock.Width / 2, y - potentialLock.Height / 2,
                    potentialLock.Width, potentialLock.Height);

                if (rect.IntersectsWith(potentialRect))
                {
                    return false;
                }
                    

            }

            return true;
        }
    }
}
