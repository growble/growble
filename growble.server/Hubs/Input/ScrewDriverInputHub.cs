﻿using growble.sim.Object.Blocks;
using growble.sim.Objects.Items.Tools;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.Hubs
{
    public partial class ServerHub : Hub
    {
		public void ScrewDriverInput(int x, int y)
        {

            //Get the tile
            var tile = this.WorldData.World.GetTile(x, y);

            if (tile.Block is DoorBlock)
            {
                DoorBlock block = tile.Block as DoorBlock;
                
                this.Clients.Caller.doorDetails(x,y, block.World);
            }
            else if (tile.Block is WorldDoorBlock)
            {
                //Check if allready owned
                if (this.WorldData.World.User == "")
                {
                    this.Clients.Caller.buyWorld(this.WorldData.World.Name);
                }
                else
                {
                    this.Clients.Caller.errorMessage(this.WorldData.World.User + " owns this world!");
                }

            }
            else if (tile.Block is SignBlock)
            {

                this.Clients.Caller.changeSign(tile.X, tile.Y, (tile.Block as SignBlock).ShoutOutMessage);
            }
        }

        public void updateDoor(int x, int y,  string world)
        {

            //Get the tile
            var tile = this.WorldData.World.GetTile(x, y);

            if (tile.Block is DoorBlock)
            {
                //Update it
                (tile.Block as DoorBlock).World = world;
            }
        }

        public void updateSign(int x, int y, string text)
        {

            //Get the tile
            var tile = this.WorldData.World.GetTile(x, y);

            if (tile.Block is SignBlock)
            {
                //Update it
                (tile.Block as SignBlock).ShoutOutMessage = text;
            }
        }

        public void buyWorld()
        {


            if (this.GetPlayer().Account.Gems > 5000)
            {
                this.GetPlayer().Account.Gems -= 5000;
                this.WorldData.World.User = this.GetPlayer().Name;

                //Own this world
                this.Clients.Caller.errorMessage("You own this world now!");
            }
        }
    }
}
