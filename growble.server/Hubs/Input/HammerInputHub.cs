﻿using growble.sim.Object.Blocks;
using growble.sim.Object.Blocks.Types;
using growble.sim.Objects.Items.Tools;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.Hubs
{
    public partial class ServerHub : Hub
    {
        public void HammerInput(int x, int y)
        {
            //validate player
            if (!this.IsPlayerWorld()) { return; }

            //Get the tile
            var tile = this.WorldData.World.GetTile(x, y);

            //Inventory
            var inventory = this.GetPlayer().Account.PlayerInventory;

            if (tile.Block is IBreakable)
            {


                if (tile.Block is ILock)
                {
                    //Remove the lock from the lock list
                    this.WorldData.World.Locks.RemoveAll(p => p.X == x && p.Y == y);
                }
                    

                //The tile can be destroyed
                this.WorldData.World.Blocks[x, y] = new AirBlock();

                this.GiveGem(1);

                //Let clients know block was destoryed
                this.Clients.All.blockDestroyed(x, y);

                //Let clients know block was destoryed
                this.Clients.All.blockSet(x, y, new AirBlock().Name);

            }
            else
            {

                this.Clients.Caller.errorMessage("You can't break that!");
            }
        }

    }
}
