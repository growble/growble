﻿using growble.sim.Object.Blocks;
using growble.sim.Object.Blocks.Types;
using growble.sim.Objects;
using growble.sim.Objects.Blocks.Core;
using growble.sim.Objects.Items.Tools;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.Hubs
{
    public partial class ServerHub : Hub
    {
        public void IBlockInput(int x, int y)
        {

            //validate player
            if (!this.IsPlayerWorld()) { return; }

            //Get the tile
            var tile = this.WorldData.World.GetTile(x, y);


            if ((tile.Block.GetType().GetInterfaces().Contains(typeof(IBreakable))))
            {


                var newBlock = Activator.CreateInstance(this.GetPlayer().Account.PlayerInventory.Hand.GetType());
                ///


                

                ///
                if (this.GetPlayer().Account.PlayerInventory.RemoveItem(
                    (this.GetPlayer().Account.PlayerInventory.Hand as IItem).Name))
                {

                    if (tile.Block is ILock)
                    {
                        //Remove the lock from the list
                        this.WorldData.World.Locks.RemoveAll(p => p.X == x && p.Y == y);
                    }
                    
                    if (newBlock is ILock)
                    {

                        if (this.IsLockValid(x, y, (ILock)newBlock))
                        {

                            (newBlock as ILock).Owner = this.GetPlayer().Name;
                            this.WorldData.World.Locks.Add(new Tile() { X = x, Y = y, Block = (IBlock)newBlock });
                        }
                        else
                        {
                            this.Clients.Caller.errorMessage("Somebody is allready locking some of those squares...");
                            return;
                        }
                    }

                    //Set the tile
                    this.WorldData.World.SetTile(x, y, newBlock as IBlock);


                    //Let clients know block was destoryed
                    this.Clients.All.blockSet(x, y, (newBlock as IBlock).Name);


                    //Send the Inventory
                    this.Clients.Caller.setInventory(this.GetPlayer().Account.PlayerInventory);

                    //If ran out of blocks - reset clothing
                    if (this.GetPlayer().Account.PlayerInventory.Hand == null)
                    {

                        //Update the player Inventory Update
                        this.Clients.Caller.playerInventoryUpdate(this.GetPlayer().Name, null,
                            this.GetPlayer().Account.PlayerInventory.Clothing);
                    }
                }
                else
                {

                    this.Clients.Caller.errorMessage("You don't have any of that block :( ");
                }

            }
            else
            {
                this.Clients.Caller.errorMessage("You can't replace that block! It's unbreakable");
            }
            
        }

    }
}
