﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin;
using growble.server;
using growble.sim.Data.Repo;

namespace growble.Hubs
{
    [HubName("worldHub")]
    public partial class ServerHub : Hub
    {
        public void Chat(string Message)
        {
            this.Clients.All.WorldMessage(this.GetPlayer().Name, Message);
        }

    }
}
