﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin;
using growble.server;
using growble.sim.Data.Repo;
using growble.sim.Objects.Items.Tools;
using growble.sim;
using growble.sim.Data;

namespace growble.Hubs
{
    public partial class ServerHub : Hub
    {
        protected WorldData WorldData { get; set; }

        public ServerHub(WorldData data)
        {
            this.WorldData = data;
        }

        public Task Authenticate(string name)
        {
            if (this.GetPlayer() != null && this.GetPlayer().Name == name)
                return Task.Delay(1);

            AccountRepo arep = new AccountRepo();
            var account = arep.GetUser(name);

            Console.Out.WriteLine("Player Authenticated " + name);
            /// Some Kind of Authentication Framework is needed
            /// - A secret key sent upon logging in to main server?
            if (account != null)
            {
                Player player = new Player();
                player.ConnectionId = this.Context.ConnectionId;
                player.Account = account;
                player.X = 30;
                player.Y = 30;
                player.VelX = 0;
                player.VelY = 0;
                player.Name = account.Name;
                player.LastGems = DateTime.Now;

                ///Make sure player has a hammer
                if (player.Account.PlayerInventory.Items.Count == 0)
                {
                    player.Account.PlayerInventory.Items.Add(new InventoryItem() { Item = new Hammer(), Quantity = 1 });
                }



                //Add Player
                this.WorldData.Players.Add(player);



                //Let other Players know
                this.PlayerAdded(player);

                lock(WorldData.World)
                {

                    //Send the world to the player
                    this.Clients.Caller.SetWorld(null, WorldData.Players, this.GetPlayer().Name);
                }

            }
            else
                Console.Out.WriteLine("Not working?");

            return Task.Delay(1);

        }

        public Task LoadMe()
        {
            return Task.Run(() =>
            {
                Task.Delay(2500);
                //Send the Inventory
                this.Clients.Caller.setInventory(this.GetPlayer().Account.PlayerInventory);


                //Update the player Inventory Update
                this.Clients.Caller.playerInventoryUpdate(this.GetPlayer().Name, this.GetPlayer().Account.PlayerInventory.Hand,
                    this.GetPlayer().Account.PlayerInventory.Clothing);

                //Send the catalog
                Catalog cat = new Catalog();
                this.Clients.Caller.initCatalog(cat.Items);


            });
        }

        public override Task OnDisconnected(bool x)
        {

            var player = this.GetPlayer();

            if (player == null)
                Console.Out.WriteLine("Unverified Account Disconnected");
            else
            {
                this.PlayerRemoved(this.GetPlayer());

                AccountRepo arep = new AccountRepo();
                UserRepo urep = new UserRepo();

                this.WorldData.Players.Remove(player); //Remove from list
                //this.WorldData.Players.TakeWhile(p => p.Name == player.Name);// Where(p => p.Name == player.Name).Take(1);

                arep.SaveUser(player.Account); //Save the player back to the database

                Console.Out.WriteLine("Player Leaving world");
            }

            return base.OnDisconnected(x);
        }

        public void LeaveWorld()
        {


            //Remove player from repo
            var player = this.GetPlayer();
            AccountRepo arep = new AccountRepo();
            UserRepo urep = new UserRepo();
            arep.SaveUser(player.Account); //Save the player back to the database
            
            this.PlayerRemoved(this.GetPlayer());

            this.WorldData.Players.Remove(player);
            Console.Out.WriteLine("Player Leaving world");

            //Leave the world
            this.Clients.Caller.leaveWorld();
        }

        public Player GetPlayer()
        {
            return WorldData.Players.Where(p => p.ConnectionId == this.Context.ConnectionId).FirstOrDefault();
        }

        /// <summary>
        /// Is this players world?
        /// </summary>
        /// <returns></returns>
        private bool IsPlayerWorld()
        {
            return true;
            //
            if (this.WorldData.World.User == this.GetPlayer().Account.Name)
                return true;
            else
                return false;
        }
    }
}
