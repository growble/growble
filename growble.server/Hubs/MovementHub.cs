﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin;
using growble.server;
using growble.sim.Data.Repo;

namespace growble.Hubs
{
    public partial class ServerHub : Hub
    {
        public void PlayerAdded(Player player)
        {
            this.Clients.All.playerAdded(player);
        }

        public void PlayerRemoved(Player player)
        {
            this.Clients.All.playerRemoved(player.Name);
        }

        public void RequestMove(int x)
        {
            if (x < 0)
            {
                this.GetPlayer().ImpX = -0.450;
            }
            else if (x > 0)
            {
                this.GetPlayer().ImpX = 0.45;
            }
            else
            {
                this.GetPlayer().ImpX = 0;
            }

        }
        
        public void RequestMoveUp(int x)
        {
            if (x < 0)
            {
                this.GetPlayer().ImpY = -0.450;
            }
            else if (x > 0)
            {
                this.GetPlayer().ImpY = 0.45;
            }
            else
            {
                this.GetPlayer().ImpY = 0;
            }

        }

        public void RequestJump()
        {

            this.GetPlayer().VelY = -0.22;
        }

    }
}
