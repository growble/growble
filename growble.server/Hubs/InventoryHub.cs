﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin;
using growble.server;
using growble.sim.Data.Repo;

namespace growble.Hubs
{
    public partial class ServerHub : Hub
    {
        public void EquipItem(int id)
        {
            this.GetPlayer().Account.PlayerInventory.SetEquipped(id);

            //Send the Inventory
            this.Clients.Caller.setInventory(this.GetPlayer().Account.PlayerInventory);

            //Update the player Inventory Update
            this.Clients.Caller.playerInventoryUpdate(this.GetPlayer().Name,this.GetPlayer().Account.PlayerInventory.Hand,
                this.GetPlayer().Account.PlayerInventory.Clothing);
        }

        public void DequipItem(int id)
        {
            this.GetPlayer().Account.PlayerInventory.Dequip(id);

            //Send the Inventory
            this.Clients.Caller.setInventory(this.GetPlayer().Account.PlayerInventory);

            //Update the player Inventory Update
            this.Clients.Caller.playerInventoryUpdate(this.GetPlayer().Name, this.GetPlayer().Account.PlayerInventory.Hand,
                this.GetPlayer().Account.PlayerInventory.Clothing);
        }

        public void TrashItem(int id)
        {

            //Send the Inventory
            this.Clients.Caller.setInventory(this.GetPlayer().Account.PlayerInventory);
        }

        public void Rewards()
        {
            RewardRepo rr = new RewardRepo();

            var r = rr.GetRewards(this.GetPlayer().Name);
            
            if( r != null && r.Count() > 0)
            {
                Console.Out.WriteLine("!!!!");

                this.Clients.Caller.seeReward(r);
            }

            
        }

        public void ClaimReward(string id)
        {

            RewardRepo rr = new RewardRepo();

            var r = rr.DeleteAndReturnReward(id);
            if (r!= null)
            {
                Console.Out.WriteLine("!");
                //Give the player the gems
                this.GetPlayer().Account.Gems += r.Gems;

                //Update the player
                Clients.Caller.SetGems(this.GetPlayer().Account.Gems);

            }
        }

        public void GiveGem(int gems)
        {
            this.GetPlayer().Account.Gems += gems;

            //Give the gems
            this.Clients.Caller.setGems(this.GetPlayer().Account.Gems);
        }
    }
}
