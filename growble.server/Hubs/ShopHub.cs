﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin;
using growble.server;
using growble.sim.Data.Repo;
using growble.sim.Object.Blocks;
using growble.sim.Data;
using growble.sim.Objects;
using growble.sim.Object.Blocks.Types;

namespace growble.Hubs
{
    [HubName("worldHub")]
    public partial class ServerHub : Hub
    {
        public void BuyItem(string name, int quantity)
        {
            if (quantity < 1 || quantity > 11)
                return;

            Catalog cat = new Catalog();
            var item = cat.Items.Where(p => p.Name == name).FirstOrDefault();

            if (item == null)
                return;
            else
            {
                var cost = (item as IBuyable).Cost * quantity;
                //Can player afford that
                if (this.GetPlayer().Account.Gems > cost )
                {
                    var getItem = Activator.CreateInstance(item.GetType()) as IItem;

                    if(this.GetPlayer().Account.PlayerInventory.AddItem(getItem, quantity))
                    {
                        this.GetPlayer().Account.Gems -= cost; //Deduct gems...
                        //Send the Inventory
                        this.Clients.Caller.setInventory(this.GetPlayer().Account.PlayerInventory);

                        //Remove Gems
                        this.Clients.Caller.SetGems(this.GetPlayer().Account.Gems);

                    }
                    

                }

                
            }
        }
    }
}
