﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin;
using growble.server;
using growble.sim.Data.Repo;

namespace growble.Hubs
{
    public partial class ServerHub : Hub
    {

        public void Chat(string message)
        {
            this.Clients.All.worldChat(this.GetPlayer().Name, message);

        }


    }
}
