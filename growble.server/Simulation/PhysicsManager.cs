﻿using growble.Hubs;
using growble.sim.Object.Blocks;
using growble.sim.Object.Blocks.Types;
using growble.sim.Objects;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.server.Physics
{
    public class PhysicsManager : IManager
    {

        public static float PlayerWidth = 0.8f;
        public static float PlayerHeight = 0.8f;
        public static float PlayerMaxVel = 0.07f;
        public static float PlayerAcell = 0.007f;

        private DateTime LastPositionUpdate { get; set; }

        public PhysicsManager(WorldData data) : base(data)
        {
            LastPositionUpdate = DateTime.Now;
        }

        public override void Update()
        {
            foreach (var player in this.WorldData.Players)
            {

                double oldX = player.X;
                double oldY = player.Y;

                this.UpdatePhsics(player);

                player.ImpY = 0.45;


                this.UpdatePhsicsEndSweep(player);

                CollisionSweepY(player, this.WorldData, oldY, oldX);

                CollisionSweepX(player, this.WorldData, oldY, oldX);
                

            }


            


            if ((DateTime.Now - LastPositionUpdate).TotalMilliseconds > 105)
            {
                ///Send Update 
                var context = this.WorldData.Hub;
                context.Clients.All.updatePlayerPositions(this.WorldData.Players);

                //context.Clients.All.SetGems(1337);
                LastPositionUpdate = DateTime.Now;

                
            }

            base.Update();
        }

        private void HandleCollision(List<Tile> tiles, Player player)
        {
            foreach (var tile in tiles)
            {
                ///Handle Shout Out
                if (tile.Block is IShoutOut)
                {
                    var context = GlobalHost.ConnectionManager.GetHubContext<ServerHub>();
                    context.Clients.Client(player.ConnectionId).infoMessage((tile.Block as IShoutOut).ShoutOutMessage);

                }
            }
        }

        private void UpdatePhsics(Player player)
        {

            //Frictional Force

            float friction = 0.004f;
            float maxvel = 1.0f;

            RectangleF playerRect = new RectangleF((float)player.X, (float)player.Y, PhysicsManager.PlayerWidth, PhysicsManager.PlayerHeight);
            var tiles = this.WorldData.World.GetWithin(playerRect);

            //Handle Collision
            this.HandleCollision(tiles, player);

            //
            var frictionTiles = tiles.Where(p => p.Block is IFriction).FirstOrDefault();
            if (frictionTiles != null)
            {

                //Attathced to a friction tile?
                friction = (float)(frictionTiles.Block as IFriction).Frictional;

                if (friction < 0)
                {
                    maxvel = -friction;
                }
            }
            else
            {

            }


            if (player.ImpX < 0)
            {
                if (player.VelX > -maxvel*PhysicsManager.PlayerMaxVel)
                    player.VelX -= PhysicsManager.PlayerAcell;
            }
            else if (player.ImpX == 0)
            {
                player.VelX *= friction;
            }
            else if (player.ImpX > 0)
            {
                if (player.VelX < maxvel*PhysicsManager.PlayerMaxVel)
                    player.VelX += PhysicsManager.PlayerAcell;
            }

            player.X += player.VelX;



            if (player.ImpY < 0)
            {
                if (player.VelY > -maxvel*PhysicsManager.PlayerMaxVel)
                    player.VelY -= PhysicsManager.PlayerAcell;
            }
            else if (player.ImpY == 0)
            {
               // player.VelY *= friction;
            }
            else if (player.ImpY > 0)
            {
                if (player.VelY < maxvel*PhysicsManager.PlayerMaxVel)
                    player.VelY += PhysicsManager.PlayerAcell;
            }

            player.Y += player.VelY;

        }

        private void UpdatePhsicsEndSweep(Player player)
        {
            RectangleF playerRect = new RectangleF((float)player.X, (float)player.Y, PhysicsManager.PlayerWidth, PhysicsManager.PlayerHeight);
            var tiles = this.WorldData.World.GetWithin(playerRect);
            //Bounce
            var bounceTiles = tiles.Where(p => p.Block is IBouncey).FirstOrDefault();
            
            foreach (var x in tiles)
            {
                if (x.Block is IKillable)
                {
                    player.X = 30;
                    player.Y = 30;
                    player.VelX = 0;
                    player.VelY = 0;
                   
                    var context = this.WorldData.Hub;
                    context.Clients.Client(player.ConnectionId).errorMessage("You Died :( ");
                }
            }

            //Update Bounce
            if (bounceTiles != null)
            {
                float bounce = (float)(bounceTiles.Block as IBouncey).Bounce;

                player.VelX = -bounce * player.VelX;
                player.VelY = -bounce * player.VelY;
            }
        }

        private bool CollisionSweepX(Player player, WorldData data, double vely, double velx)
        {
            var tiles = data.World.GetAboutPoint((int)player.X, (int)player.Y);

            RectangleF playerRect = new RectangleF((float)player.X, (float)player.Y, PhysicsManager.PlayerWidth, PhysicsManager.PlayerHeight);

            List<RectangleF> Intersections = new List<RectangleF>();

            foreach (var tile in tiles)
            {
                //If we can't walk on the blcok
                if (!(tile.Block is IWalkable))
                    if (playerRect.IntersectsWith(tile.Rectangle))
                    {
                        var x = tile.Rectangle;
                        x.Intersect(playerRect);

                        Intersections.Add(x);

                    }
            }

            if (Intersections.Count == 0)
                return true;
            else
            {
                var lw = Intersections.OrderByDescending(p => p.Width);
                var lowestWidth = lw.FirstOrDefault().Width;


                player.X = velx;

                return false;
            }


        }

        private bool CollisionSweepY(Player player, WorldData data, double vely, double velx)
        {
            var tiles = data.World.GetAboutPoint((int)player.X, (int)player.Y);

            RectangleF playerRect = new RectangleF((float)player.X, (float)player.Y, PhysicsManager.PlayerWidth, PhysicsManager.PlayerHeight);

            List<RectangleF> Intersections = new List<RectangleF>();

            foreach (var tile in tiles)
            {
                //If we can't walk on the blcok
                if (!(tile.Block is IWalkable))
                    if (playerRect.IntersectsWith(tile.Rectangle))
                    {
                        var x = tile.Rectangle;
                        x.Intersect(playerRect);

                        Intersections.Add(x);

                    }
            }

            if (Intersections.Count == 0)
                return true;
            else
            {
                var lh = Intersections.OrderByDescending(p => p.Height);
                var lowestHeight = lh.FirstOrDefault().Height;

                //if (player.VelY > 0)
                //    player.Y -= lowestHeight;
                //else
                //    player.Y += lowestHeight;
                
                player.Y = vely; 

                return false;
            }


        }
    }
}
