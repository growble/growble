﻿using growble.server.Core;
using growble.server.Hubs;
using growble.sim.Object.Blocks;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.server.Regen
{

    /// <summary>
    /// Handles Gems and Rewards
    /// </summary>
    public class RegenManager : IManager
    {
        public static int Gems = 4;
        public static float Probability = 0.03f;
        public DateTime LastRegen { get; set; }
        public int RegenMS = 8000;

        public RegenManager(WorldData wdata)
            : base(wdata)
        {
            this.LastRegen = DateTime.Now;
        }

        public override void Update()
        {

            if ((DateTime.Now - LastRegen).TotalMilliseconds < RegenMS)
                return;



            Random r = new Random((int)DateTime.Now.Ticks);
            List<RegenBlock> Blocks = new List<RegenBlock>();
            for (int i = 0; i < this.WorldData.World.Blocks.GetLength(0); i++)
            {
                for (int j = 0; j < this.WorldData.World.Blocks.GetLength(1); j++)
                {
                    if (this.WorldData.World.Blocks[i, j].Name == "Air")
                    {
                        for (int ix = i - 1; ix < i + 2; ix++)
                            for (int iy = j - 1; iy < j + 2; iy++)
                                if (ix != iy && ix > 0 && iy > 0 && ix < 100 && iy < 50)
                                {
                                    if ((iy != j && ix == i) || (iy == j && ix != i)) //No diaganolss
                                        if (this.WorldData.World.Blocks[ix, iy].Name == "Grass")
                                        {
                                            if (r.NextDouble() < RegenManager.Probability)
                                            {
                                                this.WorldData.World.SetTile(i, j, new GrassBlock());
                                                Blocks.Add(new RegenBlock() { x = i, y = j });
                                            
                                            }
                                        
                                        }
                                }
                    }
                }
            }

            Console.Out.WriteLine("Updated Blocks");

            if (Blocks.Count() > 0)
            {
                Regens t = new Regens();
                t.Block = Blocks;


                this.WorldData.Hub.Clients.All.setMassBlocks(t.Block);

                    

            }

            LastRegen = DateTime.Now;
        }

    }

    public class Regens
    {
        public List<RegenBlock> Block { get; set; }
    }

    public class RegenBlock
    {
        public int x { get; set; }
        public int y { get; set; }
    }
}
