﻿using growble.server.Hubs;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace growble.server.Grow
{
    public class LifeManager : IManager
    {
        public static int LifeEveryMS = 5000;
        public LifeManager(WorldData data) : base(data)
        {

        }

        public override void Update()
        {
            foreach (var player in this.WorldData.Players)
            {
                //if ((DateTime.Now - player.LastGems).Seconds > 5)
                //{

                    player.Life++;
                    //if (player.Life > 15)
                    //    player.Life = 15;
                    //else
                    {

                        var context = GlobalHost.ConnectionManager.GetHubContext<ServerHub>();
                        context.Clients.Client(player.ConnectionId).updateLife(30); //Update there life
                        context.Clients.All.updateLife(30); //Update there life
                        context.Clients.Client(player.ConnectionId).SetGems(666);
                    }
                    
                //}

                if (player.Life <= 0)
                {
                    this.KillPlayer(player);
                }
            }
        }

        /// <summary>
        /// Player Died
        /// </summary>
        /// <param name="player"></param>
        private void KillPlayer(Player player)
        {

        }

        public void HitDeadlyObject(Player player, int life)
        {
            
        }
    }
}
